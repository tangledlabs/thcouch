import asyncio

from thresult import ResultException

from thcouch.core.db import put_db, delete_db
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase
from thcouch.orm.decl.loader import CouchLoader
from thcouch.orm.decl.model import BaseModel


COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'
PATH_TO_FILE = 'model_test_files_example/model-example_0.toml'


#
# database
#

# create
async def create_database(database_name):
    '''
    Creates database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        putDbOk: PutDbOk
    '''
    
    # create database
    (await put_db(uri = COUCH_URI, db = database_name)).unwrap()
    
    
# delete
async def delete_database(COUCH_URI, database_name):
    '''
    Deletes database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        deleteDbOk: DeleteDbOk
    '''
    
    # delete database
    (await delete_db(uri = COUCH_URI, db = database_name)).unwrap()


#
# loader
#

# setup
def setup(database_name):
    '''
    Loads BaseModel from configuration file
    using CouchLoader
            
    parameters:
        db: CouchDatabase,
        path: str
        
    returns:
        loader: CouchLoader
    '''
    
    # create CouchClient object
    client: CouchClient = CouchClient(COUCH_URI)
    
    # create CouchDatabase object
    db: CouchDatabase = client.database(database_name).unwrap()

    # create CouchLoader object
    loader: CouchLoader = CouchLoader(db, path = PATH_TO_FILE)
    
    return loader


# create
async def create_model_instance_validation_error():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using try and except blocks
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User        
    '''
    
    # database name
    database_name = create_model_instance_validation_error.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User
     
    try:
        # create User/BaseModel object, invalid email, this will raise ResultException 
        user0: User = User(email = 'email')
    except ResultException as e:
        # handle Exception here 
        pass

    # delete database
    await delete_database(COUCH_URI, database_name)


async def create_model_instance_validation_error_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_or function 
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User    

    unwrap_or:        
        parameters:
            msg: str
            
        returns:
            msg: str     
    '''
    
    # database name
    database_name = create_model_instance_validation_error_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User
      
    # create User/BaseModel object, invalid email,
    # this will return custom error message as string
    # dosen't raise exeption
    user0: User = (User(email = 'email')).unwrap_or('Custom msg')
     
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def create_model_instance_validation_error_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_value function 
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User    

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str     
    '''
    
    # database name
    database_name = create_model_instance_validation_error_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User
      
    # create User/BaseModel object, invalid email,
    # this will return exception message as string
    # dosen't raise exeption
    user0: User = (User(email = 'email')).unwrap_value()
     
    # delete database
    await delete_database(COUCH_URI, database_name)


# update
async def update_handling_exception_with_try_except():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Adds document into database
    
    - Updates specific document from database
    by given dict with fields to be updated
    
    - Handling error using try and except blocks
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    self.update:
        parameters:
            doc: dict,
            batch: None | str = None,
            new_edits: None | bool = None
                    
        returns:
            user1_1: User
    '''
    
    # database name
    database_name = update_handling_exception_with_try_except.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # create User object 
    user: User = User(email = 'user@user.com')

    # add user object into database 
    user1: User = (await user.add()).unwrap()
  
    # try to update user by setting non-existing attribute, 'wrong attribute' in example 
    updated_doc: dict = {'email': 'user@example.com', 'wrong attribute': 'wrong value'}

    # handling error using try/except for update method 
    try:
        (await user1.update(doc = updated_doc)).unwrap()
    except KeyError as e:
        pass

    # delete database
    await delete_database(COUCH_URI, database_name)


async def update_handling_exception_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Adds document into database
    
    - Updates specific document from database
    by given dict with fields to be updated
    
    - Handling error using unwrap_or function
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    self.update:
        parameters:
            doc: dict,
            batch: None | str = None,
            new_edits: None | bool = None
                    
        returns:
            user1_1: User

    unwrap_or:      
        parameters:
            msg: str  

        returns:
            msg: str
    '''
    
    # database name
    database_name = update_handling_exception_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # create User object 
    user: User = User(email = 'user@user.com')

    # add user object into database 
    user1: User = (await user.add()).unwrap()
  
    # try to update user by setting non-existing attribute, 'wrong attribute' in example 
    updated_doc: dict = {'email': 'user@example.com', 'wrong attribute': 'wrong value'}

    # handling error using unwrap_or function for update method 
    # this will return custom error message as string
    # doesn't raise exception 
    (await user1.update(doc = updated_doc)).unwrap_or('Custom msg')
   
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def update_handling_exception_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Adds document into database
    
    - Updates specific document from database
    by given dict with fields to be updated
    
    - Handling error using unwrap_value function 
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    self.update:
        parameters:
            doc: dict,
            batch: None | str = None,
            new_edits: None | bool = None
                    
        returns:
            user1_1: User

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str 
    '''
    
    # database name
    database_name = update_handling_exception_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # create User object 
    user: User = User(email = 'user@user.com')

    # add user object into database 
    user1: User = (await user.add()).unwrap()
  
    # try to update user by setting non-existing attribute, 'wrong attribute' in example 
    updated_doc: dict = {'email': 'user@example.com', 'wrong attribute': 'wrong value'}

    # create User/BaseModel object, invalid email,
    # this will return exception message as string
    # dosen't raise exception
    (await user1.update(doc = updated_doc)).unwrap_value()
   
    # delete database
    await delete_database(COUCH_URI, database_name)


# Delete
async def delete_model_wrong_id_exception_handling():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using try and except blocks
    
    - Adds document into database
    
    - Deletes specific document from database
    by given instance of specific document
    and batch
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.delete:
        parameters:
            self: BaseModel,
            batch: None | str = None
                    
        returns:
            user_1_0_1: User
    '''
    
    # database name
    database_name = delete_model_wrong_id_exception_handling.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # create User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to database 
    user1: User = (await user.add()).unwrap()
    
    # creating second User object 
    user_not_found: User = User(email = 'second@user.com')
    
    # handling delete User from database using try and except 
    # user not found
    try:
        user_deleted: User = (await User.delete(user_not_found)).unwrap()
    except ResultException as e:
        pass

    # delete database
    await delete_database(COUCH_URI, database_name)


async def delete_model_wrong_id_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unrap_or function
    
    - Adds document into database
    
    - Deletes specific document from database
    by given instance of specific document
    and batch
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.delete:
        parameters:
            self: BaseModel,
            batch: None | str = None
                    
        returns:
            user_1_0_1: User

    unwrap_or:
        parameters:
            msg: str     

        returns:
            msg: str
    '''
    
    # database name
    database_name = delete_model_wrong_id_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # create User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to database 
    user1: User = (await user.add()).unwrap()
    
    # creating second User object 
    user_not_found: User = User(email = 'second@user.com')
    
    # handling delete User from database using unwrap_or function
    # this will return custom error message as string
    # doesn't raise exception 
    # user not found
    user_deleted: User = (await User.delete(user_not_found)).unwrap_or('Custom msg')
     
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def delete_model_wrong_id_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_value function 
    
    - Adds document into database
    
    - Deletes specific document from database
    by given instance of specific document
    and batch
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.delete:
        parameters:
            self: BaseModel,
            batch: None | str = None
                    
        returns:
            user_1_0_1: User

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''
    
    # database name
    database_name = delete_model_wrong_id_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # create User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to database 
    user1: User = (await user.add()).unwrap()
    
    # creating second User object 
    user_not_found: User = User(email = 'second@user.com')
    
    # create User/BaseModel object, invalid email,
    # this will return exception message as string
    # dosen't raise exception 
    # user not found
    user_deleted: User = (await User.delete(user_not_found)).unwrap_value()
     
    # delete database
    await delete_database(COUCH_URI, database_name)


# get
async def get_handling_exception_with_try_except():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using try and except blocks
    
    - Adds document into database
    
    - Gets a specific document by given docid and
    rev for latest document revision
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.get:        
        parameters:
            docid: str,
            rev: None | str = None 
            
        returns:
            user: User  
    '''
    
    # database name
    database_name = get_handling_exception_with_try_except.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for get model by given empty id and rev 
    try:
        user0_1: User = (await User.get(docid = '', rev = '')).unwrap()
    except KeyError as e:
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def get_handling_exception_with_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_or function
    
    - Adds document into database
    
    - Gets a specific document by given docid and
    rev for latest document revision
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.get:        
        parameters:
            docid: str,
            rev: None | str = None 
            
        returns:
            user: User  

    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''
    
    # database name
    database_name = get_handling_exception_with_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using unwrap_or
    # function for get model by given empty id and rev 
    # this will return custom error message as string
    # doesn't raise exception
    user0_1: User = (await User.get(docid = '', rev = '')).unwrap_or('Custom msg')
      
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def get_handling_exception_with_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_value function
    
    - Adds document into database
    
    - Gets a specific document by given docid and
    rev for latest document revision
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.get:        
        parameters:
            docid: str,
            rev: None | str = None 
            
        returns:
            user: User  

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str 
    '''
    
    # database name
    database_name = get_handling_exception_with_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # create User/BaseModel object, invalid email,
    # this will return exception message as string
    # doesn't raise exception
    user0_1: User = (await User.get(docid = '', rev = '')).unwrap_value()
      
    # delete database
    await delete_database(COUCH_URI, database_name)


# Find
async def find_handling_exception_with_try_except():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using try and except blocks
    
    - Adds document into database
    
    - Finds all documents from database by
    given selector/query
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.find:   
        parameters:
            selector: dict,
            limit: None | int = None,
            skip: None | int = None,
            sort: None | list[dict | str] = None,
            fields: None | list[str] = None,
            use_index: None | (str | list[str]) = None,
            r: None | int = None,
            bookmark: None | str = None,
            update: None | bool = None,
            stable: None | bool = None
                 
        returns:
            users: tuple[list[User], str, str]
    '''
    
    # database name
    database_name = find_handling_exception_with_try_except.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for find models by given selector with wrong attribute 
    selector:dict = {
        'example': 'non existing attr'
    }
    try:
        users1: tuple[list[BaseModel], str, str] = (await User.find(selector = selector)).unwrap()
    except KeyError as e:
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def find_handling_exception_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_or function
    
    - Adds document into database
    
    - Finds all documents from database by
    given selector/query
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.find:   
        parameters:
            selector: dict,
            limit: None | int = None,
            skip: None | int = None,
            sort: None | list[dict | str] = None,
            fields: None | list[str] = None,
            use_index: None | (str | list[str]) = None,
            r: None | int = None,
            bookmark: None | str = None,
            update: None | bool = None,
            stable: None | bool = None
                 
        returns:
            users: tuple[list[User], str, str]

    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''
    
    # database name
    database_name = find_handling_exception_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for find models by given selector with wrong attribute 
    selector:dict = {
        'example': 'non existing attr'
    }
    
    # example for handling error using unwrap_or function for find model
    # this will return custom error message as string
    # doesn't raise exception
    users1: tuple[list[BaseModel], str, str] = (await User.find(selector = selector)).unwrap_or('Custom msg')
        
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def find_handling_exception_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_value function 
    
    - Adds document into database
    
    - Finds all documents from database by
    given selector/query
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.find:   
        parameters:
            selector: dict,
            limit: None | int = None,
            skip: None | int = None,
            sort: None | list[dict | str] = None,
            fields: None | list[str] = None,
            use_index: None | (str | list[str]) = None,
            r: None | int = None,
            bookmark: None | str = None,
            update: None | bool = None,
            stable: None | bool = None
                 
        returns:
            users: tuple[list[User], str, str]

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''
    
    # database name
    database_name = find_handling_exception_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for find models by given selector with wrong attribute 
    selector:dict = {
        'example': 'non existing attr'
    }
    
    # create User/BaseModel object, invalid email,
    # this will return exception message as string
    # dosen't raise exception
    users1: tuple[list[BaseModel], str, str] = (await User.find(selector = selector)).unwrap_value()
        
    # delete database
    await delete_database(COUCH_URI, database_name)
   
 
# bulk_get
async def bulk_get_handling_exception_with_try_except():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using try and except blocks
    
    - Adds document into database
    
    - Bulk get can be called to query several documents in bulk.
    It is well suited for fetching a specific revision of documents,
    as replicators do for example, or for getting revision history
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_get:
        parameters:
            docs: list[dict],
            revs: None | bool = None) 
                    
        returns:
            user1_1: BaseModel
    '''
    
    # database name
    database_name = bulk_get_handling_exception_with_try_except.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for bulk_get by given dict 
    doc_dict: dict = {'id': user1._id, 'rev': user1._rev}
    
    try:
        user1_1_2: list[User] = (await User.bulk_get(docs = doc_dict)).unwrap()
    except KeyError as e:
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def bulk_get_handling_exception_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_or function
    
    - Adds document into database
    
    - Bulk get can be called to query several documents in bulk.
    It is well suited for fetching a specific revision of documents,
    as replicators do for example, or for getting revision history
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_get:
        parameters:
            docs: list[dict],
            revs: None | bool = None) 
                    
        returns:
            user1_1: BaseModel

    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''
    
    # database name
    database_name = bulk_get_handling_exception_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for bulk_get by given dict 
    doc_dict: dict = {'id': user1._id, 'rev': user1._rev}
    
    # example for handling error using unwrap_or function for bulk_get
    # this will return custom error message as string
    # doesn't raise exception
    user1_1_2: list[User] = (await User.bulk_get(docs = doc_dict)).unwrap_or('Custom msg')
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def bulk_get_handling_exception_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_value function
    
    - Adds document into database
    
    - Bulk get can be called to query several documents in bulk.
    It is well suited for fetching a specific revision of documents,
    as replicators do for example, or for getting revision history
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_get:
        parameters:
            docs: list[dict],
            revs: None | bool = None) 
                    
        returns:
            user1_1: BaseModel

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''
    
    # database name
    database_name = bulk_get_handling_exception_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for bulk_get by given dict 
    doc_dict: dict = {'id': user1._id, 'rev': user1._rev}
    
    # example for handling error using unwrap_value function for bulk_get
    # this will return exception error message as string
    # doesn't raise exception
    user1_1_2: list[User] = (await User.bulk_get(docs = doc_dict)).unwrap_value()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


# bulk_docs
async def bulk_docs_handling_exception_with_try_except():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using try and except blocks
    
    - Adds document into database
    
    - Bulk docs allows you to create and update
    multiple documents at the same time within
    a single request
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_docs:
        parameters:
            docs: list[Union[BaseModel, dict]],
            new_edits: None | bool = None)
                    
        returns:
            users_0: list[dict]
    '''
    
    # database name
    database_name = bulk_docs_handling_exception_with_try_except.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for bulk_docs by given dict 
    doc_dict: dict = {user1: {'name': 'John', 'email': 'john@example.com'}}
    
    try:
        doc_dict_list: list[dict] = (await User.bulk_docs(docs = doc_dict)).unwrap()
    except KeyError as e:
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def bulk_docs_handling_exception_with_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_or function
    
    - Adds document into database
    
    - Bulk docs allows you to create and update
    multiple documents at the same time within
    a single request
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_docs:
        parameters:
            docs: list[Union[BaseModel, dict]],
            new_edits: None | bool = None)
                    
        returns:
            users_0: list[dict]

    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''
    
    # database name
    database_name = bulk_docs_handling_exception_with_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for bulk_docs by given dict 
    doc_dict: dict = {user1: {'name': 'John', 'email': 'john@example.com'}}
    
    # example for handling error using unwrap_or function for bulk_docs
    # this will return custom error message as string
    # doesn't raise exception
    doc_dict_list: list[dict] = (await User.bulk_docs(docs = doc_dict)).unwrap_or('Custom msg')
    
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def bulk_docs_handling_exception_with_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_value function
    
    - Adds document into database
    
    - Bulk docs allows you to create and update
    multiple documents at the same time within
    a single request
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_docs:
        parameters:
            docs: list[Union[BaseModel, dict]],
            new_edits: None | bool = None)
                    
        returns:
            users_0: list[dict]

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''
    
    # database name
    database_name = bulk_docs_handling_exception_with_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    User: type = loader.User

    # creating User object 
    user: User = User(email = 'user@user.com')

    # adding user1 to databse 
    user1: User = (await user.add()).unwrap()
    
    # example for handling error using try and except for bulk_docs by given dict 
    doc_dict: dict = {user1: {'name': 'John', 'email': 'john@example.com'}}
    
    # example for handling error using unwrap_or function for bulk_docs
    # this will return exception error message as string
    # doesn't raise exception
    doc_dict_list: list[dict] = (await User.bulk_docs(docs = doc_dict)).unwrap_value()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


# validation: wrong object type
async def create_object_instance_validation_error():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseModel type
        *BaseObject type
           
    - Creates instance of Profile/BaseModel type by
    providing list of BaseObject type
    
    - Creates instance of AgentProfile/BaseObject type 
    
    - Creates instance of BrokerProfile/BaseObject type
    
    - Handling error using try and except blocks
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    Profile:
        parameters:
            agent_profile: dict
         
    AgentProfile:        
        parameters:
            x: int
            
    BrokerProfile:        
        parameters:
            y: int
    '''
    
    # database name
    database_name = create_object_instance_validation_error.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    Profile: type = loader.Profile
     
    # load Object type from file 
    AgentProfile: type = loader.AgentProfile 
    
    # load Object type from file 
    BrokerProfile: type = loader.BrokerProfile 
     
    # Instance AgentProfile 
    a0: AgentProfile = AgentProfile(x = 10)
    
    # Instance BrokerProfile 
    b0: BrokerProfile = BrokerProfile(y = 10.00)
     
    # handling error using try and except example
    try:
        # create Profile/BaseModel object, wrong object attribute, this will raise ResultException 
        profile0: Profile = Profile(agent_profile = b0)
    except ResultException as e:
        # handle Exception here 
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def create_object_instance_validation_error_unwrap_or():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseModel type
        *BaseObject type
           
    - Creates instance of Profile/BaseModel type by
    providing list of BaseObject type
    
    - Creates instance of AgentProfile/BaseObject type 
    
    - Creates instance of BrokerProfile/BaseObject type
    
    - Handling error using unwrap_or function
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    Profile:
        parameters:
            agent_profile: dict
         
    AgentProfile:        
        parameters:
            x: int
            
    BrokerProfile:        
        parameters:
            y: int

    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''
    
    # database name
    database_name = create_object_instance_validation_error_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file 
    Profile: type = loader.Profile
     
    # load Object type from file 
    AgentProfile: type = loader.AgentProfile 
    
    # load Object type from file 
    BrokerProfile: type = loader.BrokerProfile 
     
    # Instance AgentProfile 
    a0: AgentProfile = AgentProfile(x = 10)
    
    # Instance BrokerProfile 
    b0: BrokerProfile = BrokerProfile(y = 10.00)
     
    # example for handling error using unwrap_or function for find model
    # this will return custom error message as string
    # doesn't raise exception
    profile0: Profile = Profile(agent_profile = b0).unwrap_or('Custom msg')
    
    # delete database
    await delete_database(COUCH_URI, database_name)


# validation: wrong type
async def create_object_instance_validation_error_wrong_type():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseModel type
        *BaseObject type
           
    - Creates instance of Profile/BaseModel type by
    providing list of BaseObject type
    
    - Creates instance of AgentProfile/BaseObject type 
    
    - Handling error using try and except blocks
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    Profile:
        parameters:
            agent_profile: dict
         
    AgentProfile:        
        parameters:
            x: int
    '''
    
    # database name
    database_name = create_object_instance_validation_error_wrong_type.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
    
    # load Model type from file 
    Profile: type = loader.Profile
     
    # load Object type from file 
    AgentProfile: type = loader.AgentProfile 
    
    # handling error using try and except
    try:     
        # Instance AgentProfile - given wrong type - expected int given string 
        a0: AgentProfile = AgentProfile(x = '10')
    except:
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def create_object_instance_validation_error_unwrap_value():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseModel type
        *BaseObject type
           
    - Creates instance of Profile/BaseModel type by
    providing list of BaseObject type
    
    - Creates instance of AgentProfile/BaseObject type 
    
    - Handling error using unwrap_value function
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    Profile:
        parameters:
            agent_profile: dict
         
    AgentProfile:        
        parameters:
            x: int

    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''
    
    # database name
    database_name = create_object_instance_validation_error_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
    
    # load Model type from file 
    Profile: type = loader.Profile
     
    # load Object type from file 
    AgentProfile: type = loader.AgentProfile 
    
    # example for handling error using unwrap_value function for find model
    # this will return exception error message as string
    # doesn't raise exception
    a0: AgentProfile = AgentProfile(x = '10').unwrap_value()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def main():
    
    # create User/BaseModel object, invalid email
    await create_model_instance_validation_error()
    
    # try to update user by setting non-existing attribute
    await update_handling_exception_with_try_except()

    # delete User from database
    await delete_model_wrong_id_exception_handling()

    # handling error using try and except for get model by given empty id and rev
    await get_handling_exception_with_try_except()
    
    # handling error using try and except for find models by given selector with wrong attribute
    await find_handling_exception_with_try_except()

    # handling error using try and except for bulk_get by given dict
    await bulk_get_handling_exception_with_try_except()

    # handling error using try and except for bulk_docs by given dict
    await bulk_docs_handling_exception_with_try_except

    # create Profile/BaseModel object, wrong object attribute
    await create_object_instance_validation_error()

    # Instance AgentProfile given wrong type 
    await create_object_instance_validation_error_wrong_type()

    # handling error using unwrap_or function
    await create_model_instance_validation_error_unwrap_or()

    # handling error using unwrap_value function
    await create_model_instance_validation_error_unwrap_value()

    # handling error using unwrap_or function
    await update_handling_exception_unwrap_or()

    # handling error using unwrap_value function
    await update_handling_exception_unwrap_value()

    # handling error using unwrap_or function
    await delete_model_wrong_id_unwrap_or()

    # handling error using unwrap_value function
    await delete_model_wrong_id_unwrap_value()

    # handling error using unwrap_or function
    await get_handling_exception_with_unwrap_or()

    # handling error using unwrap_value function
    await get_handling_exception_with_unwrap_value()

    # handling error using unwrap_or function 
    await find_handling_exception_unwrap_or()

    # handling error using unwrap_value function 
    await find_handling_exception_unwrap_value()

    # handling error using unwrap_or function 
    await bulk_get_handling_exception_unwrap_or()

    # handling error using unwrap_value function 
    await bulk_get_handling_exception_unwrap_value()
    
    # handling error using unwrap_or function 
    await bulk_docs_handling_exception_with_unwrap_or()
    
    # handling error using unwrap_value function 
    await bulk_docs_handling_exception_with_unwrap_value()
    
    # handling error using unwrap_or function 
    await create_object_instance_validation_error_unwrap_or()
    
    # handling error using unwrap_value function 
    await create_object_instance_validation_error_unwrap_value()


asyncio.run(main())

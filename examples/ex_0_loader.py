import asyncio

from thcouch.core.db import put_db, delete_db
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase
from thcouch.orm.decl.loader import CouchLoader

PATH_TO_FILE_TOML = 'model_test_files_example/model-example_0.toml'
PATH_TO_FILE_YAML = 'model_test_files_example/model-example_0.yaml'
PATH_TO_FILE_JSON = 'model_test_files_example/model-example_0.json'
PATH_TO_FILE_JSON5 = 'model_test_files_example/model-example_0.json5'
COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'


#
# database
#

# create
async def create_database(database_name):
    '''
    Creates database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        putDbOk: PutDbOk
    '''
    
    # create database
    (await put_db(uri = COUCH_URI, db = database_name)).unwrap()
    
    
# delete
async def delete_database(database_name):
    '''
    Deletes database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        deleteDbOk: DeleteDbOk
    '''
    
    # delete database
    (await delete_db(uri = COUCH_URI, db = database_name)).unwrap()


#
# loader
#

# instantiate 
async def loader_instantiate():
    '''
    Create instnace of CouchLoader by given db and path to file
    loads configurations from file
        
    parameters:
        db: CouchDatabase,
        path: str
        
    returns:
        loader: CouchLoader
    '''

    # database name
    database_name = loader_instantiate.__name__
    
    # create database
    await create_database(database_name)
    
    # create CouchClient object - example
    client: CouchClient = CouchClient(uri = COUCH_URI)
    
    '''
    CouchClient = auto_unwrap(CouchClient)
    
    client: CouchClient = CouchClient(uri = COUCH_URI)
    '''

    # create CouchDatabase object - example
    db: CouchDatabase = client.database(db = database_name).unwrap()

    '''
    db: CouchDatabase = client.database(db = database_name)
    '''

    # loader suports several file extensions for configurations - example
    loader_toml: CouchLoader = CouchLoader(db = db, path = PATH_TO_FILE_TOML)
    loader_yaml: CouchLoader = CouchLoader(db = db, path = PATH_TO_FILE_YAML)
    loader_json: CouchLoader = CouchLoader(db = db, path = PATH_TO_FILE_JSON)
    loader_json5: CouchLoader = CouchLoader(db = db, path = PATH_TO_FILE_JSON5)
    
    '''
    CouchLoader = auto_unwrap(CouchLoader)

    loader_json5: CouchLoader = CouchLoader(db = db, path = PATH_TO_FILE_JSON5)
    '''

    # any other extension will result in rasing the CouchLoaderError exception
    # formating files by extension examples are given in model_test_files_examples folder

    # delete database
    await delete_database(database_name)


async def main():
    
    # creating loader objects by extensions
    await loader_instantiate()


asyncio.run(main())

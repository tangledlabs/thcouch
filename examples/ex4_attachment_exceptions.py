import asyncio

from thresult import ResultException

from thcouch.core.db import put_db, delete_db
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase
from thcouch.orm.decl.loader import CouchLoader
from thcouch.orm.decl.model import BaseModel
from thcouch.orm import CouchAttachment


COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'
PATH_TO_FILE = 'model_test_files_example/model-example_0.toml'


#
# database
#

# create
async def create_database(database_name):
    '''
    Creates database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        putDbOk: PutDbOk
    '''
    
    # create database
    (await put_db(uri = COUCH_URI, db = database_name)).unwrap()
    
    
# delete
async def delete_database(COUCH_URI, database_name):
    '''
    Deletes database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        deleteDbOk: DeleteDbOk
    '''
    
    # delete database
    (await delete_db(uri = COUCH_URI, db = database_name)).unwrap()


#
# loader
#

# setup
def setup(database_name):
    '''
    Loads BaseModel from configuration file
    using CouchLoader
            
    parameters:
        db: CouchDatabase,
        path: str
        
    returns:
        loader: CouchLoader
    '''
    
    # create CouchClient object
    client: CouchClient = CouchClient(COUCH_URI)
    
    # create CouchDatabase object
    db: CouchDatabase = client.database(database_name).unwrap()

    # create CouchLoader object
    loader: CouchLoader = CouchLoader(db, path = PATH_TO_FILE)
    
    return loader


async def handling_exception_when_saving_file_in_database_example():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using try and except blocks

    - add_attachment adds the attachment to a specific
    document into database
    call on instance of document
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User 

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]        
    '''

    # database name
    database_name = handling_exception_when_saving_file_in_database_example.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')
    
    # save model to database
    user1: User = (await User.add(user)).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # handling exception for add atachment functions using try/except
    # given wrong file name
    try:
        data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name)).unwrap()
    except TypeError as e:
        pass

    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def handling_exception_when_saving_file_in_database_example_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_or function
    by custom msg and wont raise exception

    - add_attachment adds the attachment to a specific
    document into database
    call on instance of document
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User 

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            data_tuple: tuple[BaseModel, CouchAttachment] 
            
    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str       
    '''

    # database name
    database_name = handling_exception_when_saving_file_in_database_example_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')
    
    # save model to database
    user1: User = (await User.add(user)).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # handling exception for add atachment functions 
    # using unwrap_or function by custom message that's
    # gonna be returned instead of raising exception
    # given wrong file name
    data_tuple: tuple[BaseModel, CouchAttachment] = (
        await user1.add_attachment(attachment_name = file_name)).unwrap_or("Custom error message")
    
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def handling_exception_when_saving_file_in_database_example_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    - Handling error using unwrap_value function
    returns exception mesage as string
    and wont raise exception

    - add_attachment adds the attachment to a specific
    document into database
    call on instance of document
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
            
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User 

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            data_tuple: tuple[BaseModel, CouchAttachment] 
            
    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str      
    '''

    # database name
    database_name = handling_exception_when_saving_file_in_database_example_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')
    
    # save model to database
    user1: User = (await User.add(user)).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # handling exception for add atachment functions 
    # using unwrap_value function
    # it returns exception message as string
    # doesn't raise exception
    # given wrong file name
    data_tuple: tuple[BaseModel, CouchAttachment] = (
        await user1.add_attachment(attachment_name = file_name)).unwrap_value()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def handling_exception_when_getting_file_in_database_example():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - get_attachment from specific documents
    from database call on instance of specific document
    
    - Handling error using try and except blocks

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    get_attachment:        
        parameters:
            attachment_name: str,
            range: str | None = None
        
        returns:
            attachment: CouchAttachment
    '''

    # database name
    database_name = handling_exception_when_getting_file_in_database_example.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1 = (await user.add()).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_att, _ = data_tuple

    file_name: str = 'wrong_name'

    # handling exception for get atachment functions using try/except
    # given wrong file name
    try:
        attachment: CouchAttachment = (await user_att.get_attachment(attachment_name = file_name)).unwrap()
    except ResultException as e:
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def handling_exception_when_getting_file_in_database_example_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - get_attachment from specific documents
    from database call on instance of specific document

    - Handling error using unwrap_or function
    by custom msg and won't raise exception
    
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    get_attachment:        
        parameters:
            attachment_name: str,
            range: str | None = None
        
        returns:
            attachment: CouchAttachment
            
    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''

    # database name
    database_name = handling_exception_when_getting_file_in_database_example_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1 = (await user.add()).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_att, _ = data_tuple

    file_name: str = 'wrong_name'

    # handling exception for get atachment functions using try/except
    # using unwrap_or function by custom message that's
    # gonna be returned instead of raising exception
    # given wrong file name
    attachment: CouchAttachment = (await user_att.get_attachment(attachment_name = file_name)).unwrap_or('Custom error message')
        
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def handling_exception_when_getting_file_in_database_example_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - get_attachment from specific documents
    from database call on instance of specific document

    - Handling error using unwrap_value function
    returns exception mesage as string
    and wont raise exception
    
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    get_attachment:        
        parameters:
            attachment_name: str,
            range: str | None = None
        
        returns:
            attachment: CouchAttachment
            
    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''

    # database name
    database_name = handling_exception_when_getting_file_in_database_example_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1 = (await user.add()).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_att, _ = data_tuple

    file_name: str = 'wrong_name'

    # handling exception for get atachment functions
    # using unwrap_value function
    # it returns exception message as string
    # doesn't raise exception
    # given wrong file name
    attachment: CouchAttachment = (await user_att.get_attachment(attachment_name = file_name)).unwrap_value()
        
    # delete database
    await delete_database(COUCH_URI, database_name)


async def handling_exception_when_updating_attachment_in_database_example():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - update_attachment updates specific attachment
    from specific document from database by given
    attachment_name and body
    call on instance of specific document
    
    - Handling error using try and except blocks

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    update_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            updated_data_tuple: tuple[BaseModel, CouchAttachment]
    '''

    # database name
    database_name = handling_exception_when_updating_attachment_in_database_example.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1: User = (await user.add()).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_att, _ = data_tuple
    
    # example of handling error using try and except by given empty attachment name and none as body
    try:
        attachment: CouchAttachment = (await user_att.update_attachment(attachment_name = '', body = None)).unwrap()
    except ResultException as e:
        pass

    # delete database
    await delete_database(COUCH_URI, database_name)
    

async def handling_exception_when_updating_attachment_in_database_example_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - update_attachment updates specific attachment
    from specific document from database by given
    attachment_name and body
    call on instance of specific document
    
    - Handling error using unwrap_or function
    by custom msg and won't raise exception

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    update_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            updated_data_tuple: tuple[BaseModel, CouchAttachment]
            
    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''

    # database name
    database_name = handling_exception_when_updating_attachment_in_database_example_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1: User = (await user.add()).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_att, _ = data_tuple
    
    # example of handling error by given empty attachment name and none as body
    # using unwrap_or function by custom message that's
    # gonna be returned instead of raising exception
    attachment: CouchAttachment = (await user_att.update_attachment(attachment_name = '', body = None)).unwrap_or('Custom error message')
    
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def handling_exception_when_updating_attachment_in_database_example_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - update_attachment updates specific attachment
    from specific document from database by given
    attachment_name and body
    call on instance of specific document
    
    - Handling error using unwrap_value function
    returns exception mesage as string
    and wont raise exception

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    update_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            updated_data_tuple: tuple[BaseModel, CouchAttachment]
            
    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''

    # database name
    database_name = handling_exception_when_updating_attachment_in_database_example_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1: User = (await user.add()).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_att, _ = data_tuple
    
    # handling exception for get atachment functions
    # using unwrap_value function
    # it returns exception message as string
    # doesn't raise exception
    # given wrong file name
    attachment: CouchAttachment = (await user_att.update_attachment(attachment_name = '', body = None)).unwrap_value()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


async def handling_exception_when_deleting_file_in_database_example():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - remove_attachment deletes specific attachment
    from specific document from database by given
    attachment_name and batch
    call on instance of specific document
    
    - Handling error using try and except blocks

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    remove_attachment:        
        parameters:
            attachment_name: str,
            batch: None | str = None
        
        returns:
            user1_2: BaseModel
    '''

    # database name
    database_name = handling_exception_when_deleting_file_in_database_example.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1: User = (await user.add()).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_no_id: User = User(email = 'noid@user.com')

    # handling exception for remove atachment functions using try/except
    # given wrong file name
    try:
        (await user_no_id.remove_attachment(attachment_name = file_name)).unwrap()
    except ResultException as e:
        pass
    
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def handling_exception_when_deleting_file_in_database_example_unwrap_or():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - remove_attachment deletes specific attachment
    from specific document from database by given
    attachment_name and batch
    call on instance of specific document
    
    - Handling error using unwrap_or function
    by custom msg and won't raise exception

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    remove_attachment:        
        parameters:
            attachment_name: str,
            batch: None | str = None
        
        returns:
            user1_2: BaseModel
            
    unwrap_or: 
        parameters:
            msg: str

        returns:
            msg: str
    '''

    # database name
    database_name = handling_exception_when_deleting_file_in_database_example_unwrap_or.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1: User = (await user.add()).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_no_id: User = User(email = 'noid@user.com')

    # handling exception for remove atachment functions
    # using unwrap_or function by custom message that's
    # gonna be returned instead of raising exception
    # given wrong file name
    (await user_no_id.remove_attachment(attachment_name = file_name)).unwrap_or('Custom error message')
        
    # delete database
    await delete_database(COUCH_URI, database_name)

 
async def handling_exception_when_deleting_file_in_database_example_unwrap_value():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - remove_attachment deletes specific attachment
    from specific document from database by given
    attachment_name and batch
    call on instance of specific document
    
    - Handling error using unwrap_value function
    returns exception mesage as string
    and wont raise exception

    User:
        parameters:
            email: str
        
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    remove_attachment:        
        parameters:
            attachment_name: str,
            batch: None | str = None
        
        returns:
            user1_2: BaseModel
            
    unwrap_value:
        in case Ok:
            returns:
                value: Any
                
        in case Err:        
            returns:
                msg: str
    '''

    # database name
    database_name = handling_exception_when_deleting_file_in_database_example_unwrap_value.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user: User = User(email = 'user0@example.com')

    # save model to database
    user1: User = (await user.add()).unwrap()

    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # save attachment to database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name = file_name, body = content)).unwrap()

    user_no_id: User = User(email = 'noid@user.com')

    # handling exception for get atachment functions
    # using unwrap_value function
    # it returns exception message as string
    # doesn't raise exception
    # given wrong file name
    (await user_no_id.remove_attachment(attachment_name = file_name)).unwrap_value()
        
    # delete database
    await delete_database(COUCH_URI, database_name)


async def main():
    
    # handling exception for add atachment
    await handling_exception_when_saving_file_in_database_example()

    # handling exception for get atachment functions
    await handling_exception_when_getting_file_in_database_example()

    # example of handling error using try and except by given empty attachment name and none as body
    await handling_exception_when_updating_attachment_in_database_example()
    
    # handling exception for remove atachment functions
    await handling_exception_when_deleting_file_in_database_example()

    # handling exception using unwrap_or function
    await handling_exception_when_saving_file_in_database_example_unwrap_or()
    
    # handling exception using unwrap_value function
    await handling_exception_when_saving_file_in_database_example_unwrap_value()
    
    # handling exception using unwrap_or function
    await handling_exception_when_getting_file_in_database_example_unwrap_or()
    
    # handling exception using unwrap_value function
    await handling_exception_when_getting_file_in_database_example_unwrap_value()
    
    # handling exception using unwrap_or function
    await handling_exception_when_updating_attachment_in_database_example_unwrap_or()
    
    # handling exception using unwrap_value function
    await handling_exception_when_updating_attachment_in_database_example_unwrap_value()
    
    # handling exception using unwrap_or function
    await handling_exception_when_deleting_file_in_database_example_unwrap_or()

    # handling exception using unwrap_value function
    await handling_exception_when_deleting_file_in_database_example_unwrap_value()


asyncio.run(main())

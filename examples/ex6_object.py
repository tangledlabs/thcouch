import asyncio

from thcouch.core.db import put_db, delete_db
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase
from thcouch.orm.decl.loader import CouchLoader


COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'
PATH_TO_FILE = 'model_test_files_example/model-example_0.toml'


#
# database
#

# create
async def create_database(database_name):
    '''
    Creates database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        putDbOk: PutDbOk
    '''
    
    # create database
    (await put_db(uri = COUCH_URI, db = database_name)).unwrap()
    
    
# delete
async def delete_database(COUCH_URI, database_name):
    '''
    Deletes database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        deleteDbOk: DeleteDbOk
    '''
    
    # delete database
    (await delete_db(uri = COUCH_URI, db = database_name)).unwrap()


#
# loader
#

# setup
def setup(database_name):
    '''
    Loads BaseModel from configuration file
    using CouchLoader
            
    parameters:
        db: CouchDatabase,
        path: str
        
    returns:
        loader: CouchLoader
    '''
    
    # create CouchClient object
    client: CouchClient = CouchClient(COUCH_URI)
    
    # create CouchDatabase object
    db: CouchDatabase = client.database(database_name).unwrap()

    # create CouchLoader object
    loader: CouchLoader = CouchLoader(db, path = PATH_TO_FILE)
    
    return loader


#
# Object
#

# Instantiate
async def instantiate_object():
    '''
    - Loads BaseObject from configuration file
    using CouchLoader
    
    - Creates instance of BaseObject type 
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    AgentProfile:
        parameters:
            x: int
    '''
    
    # database name
    database_name = instantiate_object.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
 
    # Load Object type from file
    AgentProfile: type = loader.AgentProfile

    # Instance AgentProfile/BaseObject
    a0: AgentProfile = AgentProfile(x=10)

    # delete database
    await delete_database(COUCH_URI, database_name)


# Create
async def create_object():
    '''
    - Loads BaseObject from configuration file
    using CouchLoader
    
    - Creates instance of BaseObject type

    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
    
    User:
        parameters:
            email: str
            agent_profile: AgentProfile

    AgentProfile:
        parameters:
            x: int

    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
    '''

    # database name
    database_name = create_object.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
 
    # Load Object type from file
    AgentProfile: type = loader.AgentProfile

    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)

    # load Model type from file
    User: type = loader.User
     
    # create User object by given email and agent profile as attributes
    user0: User = User(email = 'user0@example.com', agent_profile=a0)

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


# Get
async def get_object():
    '''
    - Loads BaseObject from configuration file
    using CouchLoader
    
    - Creates instance of BaseObject type

    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - Gets a specific document by given docid and
    rev for latest document revision

    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
    
    User:
        parameters:
            email: str
            agent_profile: AgentProfile

    AgentProfile:
        parameters:
            x: int

    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    User.get:        
        parameters:
            docid: str,
            rev: None | str = None 
            
        returns:
            user: User 
    '''

    # database name
    database_name = get_object.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
 
    # Load Object type from file
    AgentProfile: type = loader.AgentProfile

    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)

    # load Model type from file
    User: type = loader.User
     
    # create User object by given email and agent profile as attributes
    user0: User = User(email = 'user0@example.com', agent_profile = a0)

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # get model from database by given id and rev
    user0_1: User = (await User.get(docid = user1_0._id, rev = user1_0._rev)).unwrap()
 
    # getting object type of model
    agent_profile: AgentProfile = user0_1.agent_profile

    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
# Update
async def update_object():
    '''
    - Loads BaseObject from configuration file
    using CouchLoader
    
    - Creates instance of BaseObject type

    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - Updates specific document from database
    by given dict with fields to be updated

    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
    
    User:
        parameters:
            email: str
            agent_profile: AgentProfile

    AgentProfile:
        parameters:
            x: int

    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    self.update:
        parameters:
            doc: dict,
            batch: None | str = None,
            new_edits: None | bool = None
                    
        returns:
            user1_1: User
    '''

    # database name
    database_name = update_object.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
 
    # Load Object type from file
    AgentProfile: type = loader.AgentProfile

    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)

    # load Model type from file
    User: type = loader.User
     
    # create User object by given email and agent profile as attributes
    user0: User = User(email = 'user0@example.com', agent_profile = a0)

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()
    
    a0.x = 20

    updated_doc: dict = {'agent_profile': a0}

    # update model from database
    doc: User = (await user1_0.update(doc = updated_doc)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


# Delete
async def delete_object():
    '''
    - Loads BaseObject from configuration file
    using CouchLoader
    
    - Creates instance of BaseObject type

    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - Deletes BaseObject from specific document
    from database by deleting document's attribute
    and updating existing document without BaseObject(attribute)

    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
    
    User:
        parameters:
            email: str
            agent_profile: AgentProfile

    AgentProfile:
        parameters:
            x: int

    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    self.update:
        parameters:
            doc: dict,
            batch: None | str = None,
            new_edits: None | bool = None
                    
        returns:
            user1_1: User
    '''

    # database name
    database_name = delete_object.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
 
    # Load Object type from file
    AgentProfile: type = loader.AgentProfile

    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)

    # load Model type from file
    User: type = loader.User
     
    # create User object by given email and agent profile as attributes
    user0: User = User(email='user0@example.com', agent_profile = a0)

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    user1_0.agent_profile = None

    # set the model attribute to None and update model
    doc: User = (await user1_0.update(doc = user1_0)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


# Delete object second way
async def delete_object_2nd_way():
    '''
    - Loads BaseObject from configuration file
    using CouchLoader
    
    - Creates instance of BaseObject type

    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - Deletes specific document from database
    by given instance of specific document
    and batch

    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
    
    User:
        parameters:
            email: str
            agent_profile: AgentProfile

    AgentProfile:
        parameters:
            x: int

    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    User.delete:
        parameters:
            self: BaseModel,
            batch: None | str = None
                    
        returns:
            user_1_0_1: User
    '''

    # database name
    database_name = delete_object_2nd_way.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
 
    # Load Object type from file
    AgentProfile: type = loader.AgentProfile

    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)

    # load Model type from file
    User: type = loader.User
     
    # create User object by given email and agent profile as attributes
    user0: User = User(email='user0@example.com', agent_profile = a0)

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # 2nd way to delete object is to delete model from database
    user_1_0_1: User = (await User.delete(user1_0)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


async def main():

    # Instance AgentProfile/BaseObject
    await instantiate_object()

    # create User object by given email and agent profile as attributes
    await create_object()

    # get model from database by given id and rev
    await get_object()

    # update model from database
    await update_object()

    # delete object is to delete model from database   
    await delete_object()

    # 2nd way to delete object is to delete model from database   
    await delete_object_2nd_way()


asyncio.run(main())

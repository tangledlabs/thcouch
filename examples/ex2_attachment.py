import asyncio

from thcouch.core.db import put_db, delete_db
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase
from thcouch.orm.decl.loader import CouchLoader
from thcouch.orm.decl.model import BaseModel
from thcouch.orm import CouchAttachment


COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'
PATH_TO_FILE = 'model_test_files_example/model-example_0.toml'


#
# database
#

# create
async def create_database(database_name):
    '''
    Creates database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        putDbOk: PutDbOk
    '''
    
    # create database
    (await put_db(uri = COUCH_URI, db = database_name)).unwrap()
    
    
# delete
async def delete_database(COUCH_URI, database_name):
    '''
    Deletes database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        deleteDbOk: DeleteDbOk
    '''
    
    # delete database
    (await delete_db(uri = COUCH_URI, db = database_name)).unwrap()


#
# loader
#

# setup
def setup(database_name):
    '''
    Loads BaseModel from configuration file
    using CouchLoader
            
    parameters:
        db: CouchDatabase,
        path: str
        
    returns:
        loader: CouchLoader
    '''
    
    # create CouchClient object
    client: CouchClient = CouchClient(COUCH_URI)
    
    # create CouchDatabase object
    db: CouchDatabase = client.database(database_name).unwrap()

    # create CouchLoader object
    loader: CouchLoader = CouchLoader(db, path = PATH_TO_FILE)
    
    return loader


#
# attachment
#

# add
async def add_attachment():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database
    
    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]
    '''
    
    # database name
    database_name = add_attachment.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')
    
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # attachment
    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # add attachment to model into database
    data_tuple: tuple[BaseModel, CouchAttachment] = (await user1_0.add_attachment(attachment_name = file_name, body = content)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


# get
async def get_attachment():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - get_attachment from specific documents
    from database

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    get_attachment:        
        parameters:
            attachment_name: str,
            range: str | None = None
        
        returns:
            attachment: CouchAttachment
    '''

    # database name
    database_name = get_attachment.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # attachment
    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # add attachment to model into database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
    await user1_0.add_attachment(attachment_name = file_name, body = content)).unwrap()
    
    user1_1, _ = data_tuple
    
    # get attachment from a specific model from database
    attachment: CouchAttachment = (await user1_1.get_attachment(attachment_name = file_name)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


# update
async def update_attachment():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - update_attachment updates specific attachment
    from specific document from database by given
    attachment_name and body
    call on instance of specific document

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    update_attachment:        
        parameters:
            attachment_name: str,
            body: bytes
        
        returns:
            updated_data_tuple: tuple[BaseModel, CouchAttachment]
    '''
    
    # database name
    database_name = update_attachment.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User
    
    # create User/BaseModel object 
    user0: User = User(email = 'user0@example.com')
 
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()
    
    # attachment
    # example attachment file
    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # add attachment to model into database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
    await user1_0.add_attachment(attachment_name = file_name, body = content)).unwrap()
    
    user1_1, _ = data_tuple
        
    # example attachment file
    file_name: str = 'abc.txt'
    with open(file_name, 'w') as f:
        f.write('This is a updated attachment text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # updates attachment from a specific model from database 
    updated_data_tuple: tuple[BaseModel, CouchAttachment] = (
    await user1_1.update_attachment(attachment_name = 'New updated name', body = content)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


# get
async def delete_attachment():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database

    - add_attachment to a specific document
    from database by given attachment_name and body
    call on instance of specific document
    
    - remove_attachment deletes specific attachment
    from specific document from database by given
    attachment_name and batch
    call on instance of specific document

    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User

    add_attachment:        
        parameters:
            attachment_name: str,
            body: bytes

        returns:
            data_tuple: tuple[BaseModel, CouchAttachment]

    remove_attachment:        
        parameters:
            attachment_name: str,
            batch: None | str = None
        
        returns:
            user1_2: BaseModel
    '''

    # database name
    database_name = delete_attachment.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # attachment
    file_name: str = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # add attachment to model into database
    data_tuple: tuple[BaseModel, CouchAttachment] = (
    await user1_0.add_attachment(attachment_name = file_name, body = content)).unwrap()
    
    user1_1, _ = data_tuple
    
    # deletes the attachment from specific document from database and returns updated document without the attachment
    user1_2: BaseModel = (await user1_1.remove_attachment(attachment_name = file_name)).unwrap()

    # delete database
    await delete_database(COUCH_URI, database_name)


async def main():

    # add attachment to model into database
    await add_attachment()

    # get attachment from a specific model from database
    await get_attachment()

    # updates attachment from a specific model from database
    await update_attachment()

    # deletes the attachment from specific document from database and returns updated document without the attachment
    await delete_attachment()


asyncio.run(main())

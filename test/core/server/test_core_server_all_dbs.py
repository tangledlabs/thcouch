import pytest

from thresult import ResultException

from thcouch.core.db import put_db, delete_db, PutDbOk, DeleteDbOk
from thcouch.core.server import GetAllDbsOk, get_all_dbs


@pytest.mark.asyncio
async def test_core_server_all_dbs(couch_setup):
    '''
    Returns a list of all the databases in the CouchDB instance - sucdess
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['all_dbs_test_db1', 'all_dbs_test_db2', 'all_dbs_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()
        assert isinstance(put_db_res, PutDbOk)

    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI)).unwrap()
    assert isinstance(get_all_dbs_res, GetAllDbsOk)

    assert all(elem in get_all_dbs_res.dbs for elem in dbs_to_create)

    # remove created databases
    for db in dbs_to_create:
        res = (await delete_db(COUCH_URI, db)).unwrap()
        assert isinstance(res, DeleteDbOk)


@pytest.mark.asyncio
async def test_core_server_all_dbs_descending(couch_setup):
    '''
    Return the databases in descending order by key. Function accepts string 'True',
    boolean True or integer '1' values, any other argument passed is ignored,
    and parameter 'descending' is defaulted to false. - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['all_dbs_test_db1', 'all_dbs_test_db2', 'all_dbs_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()
        assert isinstance(put_db_res, PutDbOk)

    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, descending=True)).unwrap()

    assert all(elem in get_all_dbs_res.dbs for elem in dbs_to_create)

    # remove created databases
    for db in dbs_to_create:
        res = (await delete_db(COUCH_URI, db)).unwrap()
        assert isinstance(res, DeleteDbOk)


@pytest.mark.asyncio
async def test_core_server_all_dbs_end_key(couch_setup):
    '''
    Stop returning databases when the specified key is reached.
    end_key is wrong type, boolean instead of json/string - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['all_dbs_test_db1', 'all_dbs_test_db2', 'all_dbs_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()
        assert isinstance(put_db_res, PutDbOk)

    end_key = 2
    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, end_key=end_key)).unwrap()

    # remove created databases
    for db in dbs_to_create:
        res = (await delete_db(COUCH_URI, db)).unwrap()
        assert isinstance(res, DeleteDbOk)


@pytest.mark.asyncio
async def test_core_server_get_all_dbs_limit(couch_setup):
    '''
    Limit the number of the returned databases to the specified number - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['all_dbs_test_db1', 'all_dbs_test_db2', 'all_dbs_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()
        assert isinstance(put_db_res, PutDbOk)

    limit = 1
    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, limit=limit)).unwrap()
    assert len(get_all_dbs_res.dbs) == limit

    # remove created databases
    for db in dbs_to_create:
        res = (await delete_db(COUCH_URI, db)).unwrap()
        assert isinstance(res, DeleteDbOk)


@pytest.mark.asyncio
async def test_core_server_all_dbs_skip(couch_setup):
    '''
    Skip this number of databases before starting to return the results. Default is 0 - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI)).unwrap()

    skip: int = 1
    get_all_dbs_res_skip = (await get_all_dbs(uri=COUCH_URI, skip=skip)).unwrap()

    assert len(get_all_dbs_res.dbs) != len(get_all_dbs_res_skip.dbs)
    assert len(get_all_dbs_res_skip.dbs) == len(get_all_dbs_res.dbs) - skip


@pytest.mark.asyncio
async def test_core_server_get_all_dbs_start_key(couch_setup):
    '''
    Return databases starting with the specified key success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['all_dbs_test_db1', 'all_dbs_test_db2', 'all_dbs_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()
        assert isinstance(put_db_res, PutDbOk)

    start_key = 2
    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, start_key=start_key)).unwrap()

    # remove created databases
    for db in dbs_to_create:
        res = (await delete_db(COUCH_URI, db)).unwrap()
        assert isinstance(res, DeleteDbOk)


@pytest.mark.asyncio
async def test_core_server_all_dbs_wrong_descending(couch_setup):
    '''
    Return the databases in descending order by key. Function accepts string 'True',
    boolean True or integer '1' values, any other argument passed is ignored
    and parameter 'descending' is defaulted to false success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['all_dbs_test_db1', 'all_dbs_test_db2', 'all_dbs_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()
        assert isinstance(put_db_res, PutDbOk)

    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, descending='wrong_arg')).unwrap()

    assert all(elem in get_all_dbs_res.dbs for elem in dbs_to_create)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_server_all_dbs_fails_wrong_end_key_type(couch_setup):
    '''
    Stop returning databases when the specified key is reached.
    end_key is wrong type, boolean instead of json/string - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    end_key = False
    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, end_key=end_key)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_server_get_all_dbs_limit_fails_wrong_limit_param(couch_setup):
    '''
    Limit the number of the returned databases to the specified number - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    limit = -1
    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, limit=limit)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_server_get_all_dbs_wrong_start_key_type(couch_setup):
    '''
    Return databases starting with the specified key.
    Wrong start_key type, boolean given instead of string - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    start_key = False
    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, start_key=start_key)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_server_all_dbs_skip_fails_wrong_value(couch_setup):
    '''
    Skip this number of databases before starting to return the results. Default is 0 - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    skip = -1
    get_all_dbs_res = (await get_all_dbs(uri=COUCH_URI, skip=skip)).unwrap()
  
import pytest
from thresult import ResultException

from thcouch.core.server import GetServerOk, get_server


@pytest.mark.asyncio
async def test_core_server_server(couch_setup):
    '''
    This function tests get server function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    get_server_ = (await get_server(uri=COUCH_URI)).unwrap()
    assert isinstance(get_server_, GetServerOk) 
  
  
@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_server_server_wrong_uri(couch_setup):
    '''
    This function tests get server function when wrong uri passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    get_server_ = (await get_server(uri='COUCH_URI')).unwrap()

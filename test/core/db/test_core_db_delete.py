import pytest

from thresult import ResultException

from thcouch.core.db import DeleteDbOk, delete_db


@pytest.mark.asyncio
async def test_core_db_delete(couch_setup):
    '''
    Demonstrate successful deletion of database - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    deleted_db = (await delete_db(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()
    assert isinstance(deleted_db, DeleteDbOk)
    

@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_delete_wrong_uri(couch_setup):
    '''
    Demonstrate unsuccessful deletion of database using wrong uri - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    deleted_db = (await delete_db(uri='COUCH_URI', db=COUCH_DATABASE)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_delete_not_found(couch_setup):
    '''
    Demonstrate unsuccessful deletion of database using wrong database name - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    deleted_db = (await delete_db(uri=COUCH_URI, db='COUCH_DATABASE')).unwrap()


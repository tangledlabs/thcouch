import pytest

from thresult import ResultException

from thcouch.core.db import BulkDocsOk, bulk_docs


@pytest.mark.asyncio
async def test_core_db_bulk_docs(couch_setup):
    '''
    This function tests bulk docs function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    docs = [{'name': 'John', 'email': 'john@example.com'}, {'name': 'Steve', 'email': 'steve@example.com'}]

    results = (await bulk_docs(uri=COUCH_URI, db=COUCH_DATABASE, docs=docs)).unwrap()
    assert isinstance(results, BulkDocsOk)
    results = results.results

    for i in range(len(results)):
        docs[i]['_id'] = results[i]['id']
        docs[i]['_rev'] = results[i]['rev']

    docs[0]['name'] = 'John changed name'
    docs[1]['name'] = 'Steve changed name'

    results = (await bulk_docs(uri=COUCH_URI, db=COUCH_DATABASE, docs=docs)).unwrap()
    assert isinstance(results, BulkDocsOk)
    results = results.results

    for i in range(len(docs)):
        assert docs[i]['_id'] == results[i]['id']
        assert docs[i]['_rev'] != results[i]['rev'] 
        docs[i]['_rev'] = results[i]['rev']

    docs[0]['_deleted'] = True
    docs[1]['_deleted'] = True

    results = (await bulk_docs(uri=COUCH_URI, db=COUCH_DATABASE, docs=docs)).unwrap()
    assert isinstance(results, BulkDocsOk)
    
    
@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_bulk_docs_wrong_new_edits(couch_setup):
    '''
    This function tests bulk docs function - expecting exception while given wrong new edits
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    docs = [{'name': 'John', 'email': 'john@example.com'}, {'name': 'Steve', 'email': 'steve@example.com'}]

    results = (await bulk_docs(uri=COUCH_URI, db=COUCH_DATABASE, docs=docs, new_edits='wrong_data')).unwrap()
        

@pytest.mark.xfail(raises=ResultException)     
@pytest.mark.asyncio
async def test_core_db_bulk_docs_wrong_database(couch_setup):
    '''
    This function tests bulk docs function - expecting exception while given wrong database
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    docs = [{'name': 'John', 'email': 'john@example.com'}, {'name': 'Steve', 'email': 'steve@example.com'}]

    results = (await bulk_docs(uri=COUCH_URI, db='COUCH_DATABASE', docs=docs)).unwrap()

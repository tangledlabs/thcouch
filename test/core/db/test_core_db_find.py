import pytest

from thresult import ResultException

from thcouch.core.db import FindDbOk, find_db


@pytest.mark.asyncio
async def test_core_db_find_ok(couch_setup):
    '''
    This function tests find_db function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    
    docs = (await find_db(uri=COUCH_URI,
                          db=COUCH_DATABASE,
                          selector={})).unwrap()
    
    assert isinstance(docs, FindDbOk)

    docs = (await find_db(uri=COUCH_URI,
                          db=COUCH_DATABASE,
                          selector={},
                          limit=2)).unwrap()
    
    assert isinstance(docs, FindDbOk)

    docs = (await find_db(uri=COUCH_URI,
                          db=COUCH_DATABASE,
                          selector={},
                          skip=2)).unwrap()
    
    assert isinstance(docs, FindDbOk)

    docs = (await find_db(uri=COUCH_URI,
                          db=COUCH_DATABASE,
                          selector={},
                          fields=['_id', '_rev', 'y'])).unwrap()
    
    assert isinstance(docs, FindDbOk)
    
    docs = (await find_db(uri=COUCH_URI,
                          db=COUCH_DATABASE,
                          selector={},
                          use_index='x_name_index')).unwrap()
    
    assert isinstance(docs, FindDbOk)
    
    docs = (await find_db(uri=COUCH_URI,
                          db=COUCH_DATABASE,
                          selector={},
                          update=False)).unwrap()
    
    assert isinstance(docs, FindDbOk)
    
    docs = (await find_db(uri=COUCH_URI,
                          db=COUCH_DATABASE,
                          selector={},
                          stable=False)).unwrap()
    
    assert isinstance(docs, FindDbOk)
    

@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_find_wrong_uri(couch_setup):
    '''
    This function tests unsuccessfull find function with wrong uri - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    docs = (await find_db(uri='COUCH_URI',
                        db=COUCH_DATABASE,
                        selector={})).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_find_invalid_request(couch_setup):
    '''
    This function tests find function, - expected exception
    400 Bad Request – Invalid request, 
    selector is set to None
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    docs = (await find_db(uri='COUCH_URI',
                        db=COUCH_DATABASE,
                        selector=None)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_find_database_not_found(couch_setup):
    '''
    This function tests find function, - expected exception
    404 - Requested database not found
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    docs = (await find_db(uri=COUCH_URI,
                        db='COUCH_DATABASE',
                        selector={})).unwrap()

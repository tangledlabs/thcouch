import pytest

from thresult import ResultException

from thcouch.core.db import GetAllDocsOk, get_all_docs


@pytest.mark.asyncio
async def test_core_db_all_docs_ok(couch_setup):
    '''
    This function is for testing get_all_docs function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    
    docs = (await get_all_docs(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()
    assert isinstance(docs, GetAllDocsOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_all_docs_fails_non_existing_db_name_parameter(couch_setup):
    '''
    This function is for testing get_all_docs function when non existing db name parameter passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    docs = (await get_all_docs(uri=COUCH_URI, db='COUCH_DATABASE')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_all_docs_fails_wrong_uri_parameter(couch_setup):
    '''
    This function is for testing get_all_docs function when wrong uri parameter passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    docs = (await get_all_docs(uri='COUCH_URI', db=COUCH_DATABASE)).unwrap()

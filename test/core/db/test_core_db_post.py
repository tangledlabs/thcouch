import pytest

from thresult import ResultException

from thcouch.core.db import PostDbOk, post_db


@pytest.mark.asyncio
async def test_core_db_post_ok(couch_setup):
    '''
    This function tests post db function for creating the document - sucess
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    doc = {'x': 10}

    res = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()
    assert isinstance(res, PostDbOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_post_fails_non_existing_db_name(couch_setup):
    '''
    This function tests post db function for creating the document - fails with 404 Not Found - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    res = (await post_db(uri=COUCH_URI, db='non_existing_db_name', doc={'x': 10})).unwrap()

@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_post_fails_invalid_uri(couch_setup):
    '''
    This function tests post db function for creating the document - fails when send wrong uri - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    doc = {'x': 10}

    res = (await post_db(uri='COUCH_URI', db=COUCH_DATABASE, doc=doc)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_post_fails_invalid_db_name_parameter_format(couch_setup):
    '''
    This function tests post db function for creating the document with invalid db name param format- fails with 400 Bad Request - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    doc = {'x': 10}

    res = (await post_db(uri=COUCH_URI, db='%%%%%%Invalid_db_name_format', doc=doc)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_post_fails_empty_string_db_name_parameter(couch_setup):
    '''
    This function tests post db function for creating the document with invalid db name param format- fails with 400 Bad Request - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    doc = {'x': 10}

    res = (await post_db(uri=COUCH_URI, db='', doc=doc)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_post_fails_document_with_same_id_alerady_exists(couch_setup):
    '''
    This function tests post db function for creating the document - fails with 409 Conflict - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc={'x': 10})).unwrap()

    existing_doc = {
        '_id': res.id,
        'x': 20,
    }

    res_2 = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=existing_doc)).unwrap()


@pytest.mark.asyncio
async def test_core_db_post_ok_batch(couch_setup):
    '''
    This function tests post db function for creating the document - sucess
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    doc = {'x': 10}

    res = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc, batch='Ok')).unwrap()
    assert isinstance(res, PostDbOk)

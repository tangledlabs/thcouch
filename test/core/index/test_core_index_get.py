import pytest

from thresult import ResultException

from thcouch.core.index import GetIndexOk, post_index, get_index


@pytest.mark.asyncio
async def test_core_index_get(couch_setup):
    '''
    This function tests get index function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri=COUCH_URI,
                            db=COUCH_DATABASE,
                            index={'fields': ['collection', 'test_index']})).unwrap()
    
    get_db_index = (await get_index(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()
    assert isinstance(get_db_index, GetIndexOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_get_fails_wrong_url(couch_setup):
    '''
    This function tests get index function when wrong uri passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    get_db_index = (await get_index(uri='COUCH_URI', db=COUCH_DATABASE)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_get_fails_wrong_db(couch_setup):
    '''
    This function tests get index function when wrong db param passed - exception expected
    '''    
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    get_db_index = (await get_index(uri=COUCH_URI, db='COUCH_DATABASE')).unwrap()

import pytest

from thresult import ResultException

from thcouch.core.doc import DeleteDocOk, put_doc, delete_doc


@pytest.mark.asyncio
async def test_delete_doc(couch_setup):
    '''
    This function tests delete doc function - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    id = doc.id
    rev=doc.rev
    doc_deleted = (await delete_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id,rev=rev)).unwrap()
    assert isinstance(doc_deleted, DeleteDocOk)


@pytest.mark.asyncio
async def test_delete_doc_with_batch(couch_setup):
    '''
    This function tests delete doc function  with batch - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()  
    id = doc.id
    rev = doc.rev
    doc_deleted = (await delete_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, rev=rev, batch='ok')).unwrap()
    assert isinstance(doc_deleted, DeleteDocOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_delete_doc_wrong_uri(couch_setup):
    '''
    This function tests unsuccessful delete doc function with wrong uri - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc_deleted = (await delete_doc(uri='COUCH_URI', db=COUCH_DATABASE, docid=None, rev=None)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_delete_doc_invalid_parameters(couch_setup):
    '''
    This function tests delete doc function, - expected exception
    400 - Invalid request body or parameters,
    rev is empty string
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    id: str = doc.id
    doc_deleted = (await delete_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, rev='')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_delete_doc_database_doesnt_exists(couch_setup):
    '''
    This function tests delete doc function, - expected exception
    404 - specified database doesn’t exists
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    id: str = doc.id
    rev: str = doc.rev
    doc_deleted = (await delete_doc(uri=COUCH_URI, db=COUCH_DATABASE + 'name', docid=id, rev=rev)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_delete_doc_document_id_doesnt_exists(couch_setup):
    '''
    This function tests delete doc function, - expected exception
    404 - specified document ID doesn’t exists
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    wrong_id: str = doc.id + '65'
    rev: str = doc.rev
    doc_deleted = (await delete_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=wrong_id, rev=rev)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_delete_doc_conflict(couch_setup):
    '''
    This function tests delete doc function, - expected exception
    409 - conflict specified revision is not latest for target document
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    id = doc.id
    rev_not_latest = doc.rev
    rev = rev_not_latest
    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, doc={'x': 10}, rev=rev)).unwrap()
    doc_deleted = (await delete_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id,rev=rev_not_latest)).unwrap()

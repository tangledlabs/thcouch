import pytest

from thresult import ResultException

from thcouch.core.doc import PutDocOk, put_doc


@pytest.mark.asyncio
async def test_put_doc(couch_setup):
    '''
    This function tests put doc function for creating the document - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    assert isinstance(doc, PutDocOk)


@pytest.mark.asyncio
async def test_put_doc_with_batch(couch_setup):
    '''
    This function tests put doc function for creating the document with batch - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10}, batch='ok')).unwrap()
    assert isinstance(doc, PutDocOk)


@pytest.mark.asyncio
async def test_put_doc_with_new_edits_true(couch_setup):
    '''
    This function tests put doc function for creating the document with new_edits=true - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10}, new_edits=True)).unwrap()
    assert isinstance(doc, PutDocOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_put_doc_wrong_uri(couch_setup):
    '''
    This function tests put doc function for creating the document, wrong db uri - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri='COUCH_URI', db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_put_doc_error_invalid_request_body(couch_setup):
    '''
    This function tests put doc function for creating the document, 
    400 - invalid request body or parameters, doc is None - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='123', doc=None)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_put_doc_error_specified_database_doesnt_exists(couch_setup):
    '''
    This function tests put doc function for creating the document, 
    404 - specified database doesn’t exists - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE + 'name', docid='123', doc={'x': 10})).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_put_doc_error_document_id_doesnt_exists(couch_setup):
    '''
    This function tests put doc function for creating the document, 
    404 - specified document ID doesn’t exists - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='100', doc={'x': "10", 'y': '20'})).unwrap()    
    wrong_id: str = doc.id + 'ab'
    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE + 'name', docid=wrong_id, doc={'x': 10}, rev=doc.rev)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_put_doc_error_rev(couch_setup):
    '''
    This function tests put doc function for creating the document, 
    400 - invalid request body or parameters, rev is empty string - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='100', doc={'x': "10", 'y': '20'})).unwrap()
    rev = ''
    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='100', doc={'x': 10}, rev=rev)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_put_doc_error_conflict(couch_setup):
    '''
    This function tests put doc function for creating the document, 
    409 - conflict specified revision is not latest for target document - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    # called put_doc function to create the doc for find function
    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='100', doc={'x': "10", 'y': '20'})).unwrap()
    rev_not_latest = doc.rev
    id = doc.id
    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, doc={'x': 10}, rev=doc.rev)).unwrap()
    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='123', doc={'x': '10', 'y': '20'}, rev=rev_not_latest)).unwrap()


@pytest.mark.asyncio
async def test_create_update_doc(couch_setup):
    '''
    This function tests creating the doc with db post and updating with doc_put - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='100', doc={'x': "10"})).unwrap()
    rev = doc.rev
    id = doc.id
    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, doc={'y': '20'}, rev=rev)).unwrap()
    assert isinstance(doc, PutDocOk)

import pytest

from thresult import ResultException

from thcouch.core.db import post_db
from thcouch.core.attachment import PutAttachmentOk, put_attachment, DeleteAttachmentOk, delete_attachment


@pytest.mark.asyncio
async def test_core_attachment_delete(couch_setup):
    '''
    This function tests delete attachment function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')
    
    with open(file_name, 'rb') as reader:
        content = reader.read()

    # put_attachment function is called to create a attachment so delete attachment function can be called to delete that attachment
    put_att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    id = put_att.id
    rev = put_att.rev
    assert isinstance(put_att, PutAttachmentOk)

    del_att = (await delete_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attname=file_name, rev=rev)).unwrap()
    assert isinstance(del_att, DeleteAttachmentOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_delete_catch_exception_given_wrong_uri(couch_setup):
    '''
    This function tests delete attachment function - expecting exception while given wrong uri
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    del_att = (await delete_attachment(uri="COUCH_URI", db=COUCH_DATABASE, docid=id, attname='abc.txt', rev=rev)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_delete_catch_exception_given_wrong_id(couch_setup):
    '''
    This function tests delete attachment function - expecting exception while given wrong id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    del_att = (await delete_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid="id", attname='abc.txt', rev=rev)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_delete_catch_exception_given_wrong_batch(couch_setup):
    '''
    This function tests delete attachment function -expecting exceptions while given wrong batch
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    del_att = (await delete_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', attname='abc.txt', rev=rev, batch='error')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_delete_catch_exception_given_wrong_rev(couch_setup):
    '''
    This function tests delete attachment function - expecting exceptions while rev not passed - conflict
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc =  (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # put_attachment function is called to create a attachment so delete attachment function can be called to delete that attachment
    put_att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    att_id = put_att.id
    att_rev = put_att.rev
    assert isinstance(put_att, PutAttachmentOk)

    del_att = (await delete_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=att_id, attname='abc.txt')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_delete_catch_exception_given_wrong_param(couch_setup):
    '''
    This function tests delete attachment function - expecting exceptions - while given wrong parameters
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc =  (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # put_attachment function is called to create a attachment so delete attachment function can be called to delete that attachment
    put_att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    att_id = put_att.id
    att_rev = put_att.rev
    assert isinstance(put_att, PutAttachmentOk)

    del_att = (await delete_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=att_id, attname='abc.txt', rev='wrong_rev')).unwrap()

import pytest

from thresult import ResultException

from thcouch.core.db import post_db
from thcouch.core.attachment import PutAttachmentOk, put_attachment, HeadAttachmentOk, head_attachment


@pytest.mark.asyncio
async def test_core_attachment_head(couch_setup):
    '''
    This function tests head attachment function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # put_attachment function is called to create a attachment so head attachment function can be put on that attachment
    attachment = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    assert isinstance(attachment, PutAttachmentOk)

    head_att = (await head_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attname=file_name)).unwrap()
    assert isinstance(head_att, HeadAttachmentOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_head_catch_exceptions_wrong_uri(couch_setup):
    '''
    This function tests head attachment function - expecting exceptions while given wrong uri
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    head_att = (await head_attachment(uri="COUCH_URI", db=COUCH_DATABASE, docid='id', attname='file_name')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_head_catch_exception_wrong_id(couch_setup):
    '''
    This function tests head attachment function - expecting exceptions while given wrong id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    head_att = (await head_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='id', attname='file_name')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_head_catch_exception_given_wrong_rev(couch_setup):
    '''
    This function tests head attachment function - expecting exceptions while given wrong rev
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    head_att = (await head_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='id', attname='file_name', rev='error')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_head_attn_name_param_empty_string(couch_setup):
    '''
    This function tests head attachment function - expecting exception when attname param empty string
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    head_att = (await head_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='id', attname='')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_head_docid_param_empty_string(couch_setup):
    '''
    This function tests head attachment function - expecting exception when docid param empty string
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    head_att = (await head_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='', attname='file_name')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_head_db_param_empty_string(couch_setup):
    '''
    This function tests head attachment function - expecting exception when db param empty string
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    head_att = (await head_attachment(uri=COUCH_URI, db='', docid='id', attname='file_name')).unwrap()


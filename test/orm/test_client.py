import pytest

from thresult import ResultException, Ok, Err

from thcouch.orm import CouchClient, CouchDatabase


#
# client
#
@pytest.mark.asyncio
async def test_orm_client_wait(couch_setup):
    '''
    This function tests client wait - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)

    match res := (await client.wait()):
        case Ok():
            pass
        case Err():
            raise Exception

    # assert isinstance(res, CouchClient)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_orm_client_wait_wrong_uri(couch_setup):
    '''
    This function tests client wait - expected exception while given wrong uri
    '''
    client = CouchClient('COUCH_URI')

    res = (await client.wait()).unwrap()


@pytest.mark.asyncio
async def test_orm_client_wait_timeout(couch_setup):
    '''
    This function tests client wait - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)

    timeout: float = 2
    res = (await client.wait(timeout)).unwrap()


#
# database
#
@pytest.mark.asyncio
async def test_orm_client_database(couch_setup):
    '''
    This function tests client database - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)

    db = client.database(COUCH_DATABASE).unwrap()

    assert isinstance(db, CouchDatabase)

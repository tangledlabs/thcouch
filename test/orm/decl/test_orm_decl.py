import pytest
import random

from copy import copy, deepcopy
from toml import TomlDecodeError
from json.decoder import JSONDecodeError
from yaml.parser import ParserError

from thresult import ResultException

from thcouch.orm import CouchClient, CouchDatabase, CouchAttachment
from thcouch.orm.decl import BaseModel, BaseIndex, FileConfigError, CouchLoaderError, CouchLoader


#
# # MODEL
#

#
# create database
#
@pytest.mark.asyncio
async def test_orm_decl_loader_model_basic(couch_setup):
    '''
    Loader creates dynamically Model type/class, testing function add, get, all and find - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User: type = loader.User
        user0: User
        user1: User

        # user0, instante User type, and validate
        user0: BaseModel = User(email='user0@example.com')
        user1: BaseModel = User(email='user1@example.com')

        # add user0
        user0_0: User
        user0_0 = (await User.add(user0)).unwrap()
        assert isinstance(user0_0, BaseModel)

        # add user1
        user1_0: User
        user1_0 = (await User.add(user1)).unwrap()
        assert isinstance(user1_0, BaseModel)

        # get user0_0 - id and rev parameters passed, returns specified revision
        user0_1: User
        user0_1 = (await User.get(docid=user0_0._id, rev=user0_0._rev)).unwrap()
        assert isinstance(user0_1, BaseModel)

        # get user1_0 - id parameter passed, returns latest revision
        user1_1: User
        user1_1 = (await User.get(docid=user1_0._id)).unwrap()
        assert isinstance(user1_1, BaseModel)

        # all document
        users0: list[BaseModel]
        users0 = (await User.all()).unwrap()
        assert isinstance(users0, list)
        assert all(isinstance(elem, BaseModel) for elem in users0)

        # find document
        users1: list[User]

        selector = {
            'usertype': 'company'
        }

        users1: tuple[list[BaseModel], str, str] = (await User.find(selector=selector)).unwrap()
        assert isinstance(users1, tuple)
        assert all(isinstance(elem, BaseModel) for elem in users1[0])


@pytest.mark.asyncio
async def test_orm_decl_getitem(couch_setup):
    '''
    testing getitem function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User: type = loader.User
        user0: User
        user1: User

        # user0, instantiate User type, and validate
        user0: BaseModel = User(email='user0@example.com')
        email_ = user0.__getitem__('email')
        assert isinstance(email_, str)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_validate_error(couch_setup):
    '''
    Test validation error - excepted exception while given invalid email format
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User: type = loader.User
    user0: User

    User(email='#user.com')


@pytest.mark.asyncio
async def test_orm_decl_delete(couch_setup):
    '''
    Testing the delete function for document - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User: type = loader.User
        user1: User

        # user1, instantiate User type, and validate
        user1: BaseModel = User(email='user1@example.com')

        # add user1
        user1_0: User
        user1_0 = (await User.add(user1)).unwrap()

        # delete document (user1_0)
        user_1_0_1 = (await User.delete(user1_0)).unwrap()
        assert isinstance(user_1_0_1, BaseModel)


@pytest.mark.asyncio
async def test_orm_decl_update_and_delete(couch_setup):
    '''
    Testing the delete function for document - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = ['orm/decl/model_test_files/yaml/model-0.yaml']

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User: type = loader.User
        user1: User

        # user, instantiate User type, and validate
        user1: BaseModel = User(email='user1@example.com')

        # add user
        added_user = (await User.add(user1)).unwrap()

        # update user
        update_doc = {'email': 'updated_user@example.com', 'phone': '0800-22-33-11'}
        updated_user = (await User.update(added_user, doc=update_doc)).unwrap()
        assert isinstance(updated_user, User)
        assert added_user._rev != updated_user._rev

        # delete user
        deleted_user = (await User.delete(updated_user)).unwrap()
        assert isinstance(deleted_user, User)
        assert updated_user._rev != deleted_user._rev


#
# create database
#
@pytest.mark.asyncio
async def test_orm_decl_loader_model(couch_setup):
    '''
    Loader creates dynamically Model type/class, find function tested - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user1: BaseModel = User(email=f'user{random.randint(0, 100)}@example.com')
        assert isinstance(user1, BaseModel)

        #
        # declarative add document
        #
        user1_0: User = (await user1.add()).unwrap()
        assert isinstance(user1_0, BaseModel)

        #
        # declarative get document
        #
        user1_1: User = (await User.get(docid=user1_0._id, rev=user1_0._rev)).unwrap()
        assert isinstance(user1_1, BaseModel)

        # #
        # # declarative all document
        # #
        users: list[User] = (await User.all()).unwrap()
        assert all(isinstance(el, BaseModel) for el in users)

        #
        # declarative find docs
        #
        selector = {
            'usertype': 'company'
        }

        users_tuple: tuple = (await User.find(selector)).unwrap()
        users_1, bookmark, warning = users_tuple
        assert isinstance(users_1, list)
        assert isinstance(bookmark, str)
        assert isinstance(warning, str)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_missing_required_param(couch_setup):
    '''
    Testing validation missing required param - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-2.toml')
    User = loader.User

    User(email='user@user.com')


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_wrong_type_param(couch_setup):
    '''
    Testing validation given wrong name parameter type - expected exception 
    '''

    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-2.toml')
    User = loader.User

    User(email='user@user.com', name=[])


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_wrong_value(couch_setup):
    '''
    Testing validation given wrong field value - expected exception
    '''

    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-2.toml')
    User = loader.User

    User(email='user@user.com', usertype='personal')


@pytest.mark.asyncio
async def test_orm_decl_get_item(couch_setup):
    '''
    Testing get item - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user0: BaseModel = User(email='user@user.com')
        assert user0.__getitem__('email')
        assert isinstance(user0.__getitem__('email'), str)


#
# model bulk_docs
#
@pytest.mark.asyncio
async def test_orm_decl_bulk_docs_combined_models_and_dicts_list_passed(couch_setup):
    '''
    Testing declarative bulk_docs function when combined list of models and dict passed - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user1: BaseModel = User(email='user1@user.com')
        user2: BaseModel = User(email='user2@user.com')

        doc_list: list[BaseModel] = [user1,
                                     user2,
                                     {'name': 'John', 'email': 'john@example.com'},
                                     {'name': 'Steve', 'email': 'steve@example.com'}]

        doc_dict_list: list[dict] = (await User.bulk_docs(docs=doc_list)).unwrap()
        assert isinstance(doc_dict_list, list)
        assert len(doc_dict_list) == len(doc_list)
        assert all(isinstance(elem, dict | BaseModel) for elem in doc_dict_list)
        assert all(k in elem for k in ('ok', 'id', 'rev') for elem in doc_dict_list)


@pytest.mark.asyncio
async def test_orm_decl_bulk_docs_update_and_create_docs(couch_setup):
    '''
    Testing bulk doc function - update existing and create new documents - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user1: BaseModel = User(email='user1@user.com')
        user1_created: User = (await user1.add()).unwrap()
        user2: BaseModel = User(email='user2@user.com')
        user2_created: User = (await user2.add()).unwrap()

        user1_created.ns['email'] = 'one@example.com'
        user2_created.ns['email'] = 'two@example.com'

        doc_list: list[BaseModel] = [user1_created,
                                     user2_created,
                                     {'name': 'John', 'email': 'john@example.com'},
                                     {'name': 'Steve', 'email': 'steve@example.com'}]

        doc_dict_list: list[dict] = (await User.bulk_docs(docs=doc_list)).unwrap()
        assert isinstance(doc_dict_list, list)
        assert len(doc_dict_list) == len(doc_list)
        assert all(isinstance(elem, dict) for elem in doc_dict_list)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=TypeError, strict=True)
async def test_orm_decl_bulk_docs_wrong_element_type_passed(couch_setup):
    '''
    Testing declarative bulk get function - return multiple models by given wrong element type - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User = loader.User

    doc_list = [
        {'name': 'John', 'email': 'john@example.com'},
        {'name': 'Steve', 'email': 'steve@example.com'},
        ('Peter', 'peter@example.com')
    ]

    (await User.bulk_docs(docs=doc_list)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=Exception, strict=True)
async def test_orm_decl_bulk_docs_empty_list(couch_setup):
    '''
    Testing bulk docs function by given empty list - expected Exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User = loader.User

    (await User.bulk_docs([])).unwrap()


#
# bulk_get model
#
@pytest.mark.asyncio
async def test_orm_decl_bulk_get(couch_setup):
    '''
    Testing declarative bulk_get function - return multiple models by given model list - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user1: BaseModel = User(email='user1@user.com')
        user2: BaseModel = User(email='user2@user.com')

        user1_add = (await user1.add()).unwrap()
        user2_add = (await user2.add()).unwrap()

        doc1 = {'id': user1_add._id, 'rev': user1_add._rev}
        doc2 = {'id': user2_add._id, 'rev': user2_add._rev}
        doc_list: list[dict] = [doc1, doc2]

        user1_1_2 = (await User.bulk_get(docs=doc_list)).unwrap()
        assert isinstance(user1_1_2, list)
        assert len(user1_1_2) == len(doc_list)
        assert all(isinstance(elem, BaseModel) for elem in user1_1_2)


@pytest.mark.asyncio
async def test_orm_decl_bulk_get_returns_list_of_correct_models(couch_setup):
    '''
    Testing declarative bulk get function - return only list of models found in database - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user1: BaseModel = User(email='user1@user.com')
        user1_add: BaseModel = (await user1.add()).unwrap()
        user2: BaseModel = User(email='user2@user.com')
        user2_add: BaseModel = (await user2.add()).unwrap()

        doc1 = {'id': user1_add._id, 'rev': user1_add._rev}
        doc2 = {'id': user2_add._id, 'rev': user2_add._rev}
        correct_docs_list: list[dict] = [doc1, doc2]

        doc3 = {'id': 'wrong_id_3', 'rev': 'wrong_rev_3'}
        doc4 = {'id': 'wrong_id_4', 'rev': 'wrong_rev_4'}
        wrong_docs_list: list[dict] = [doc3, doc4]

        merged_docs_list: list[dict] = [*correct_docs_list, *wrong_docs_list]

        model_instances: list[BaseModel | None] = (await User.bulk_get(docs=merged_docs_list)).unwrap()
        assert isinstance(model_instances, list)
        assert len(model_instances) == len(merged_docs_list)
        assert all(isinstance(elem, BaseModel | None) for elem in model_instances)


#
# update model
#
@pytest.mark.asyncio
async def test_orm_decl_update_doc(couch_setup):
    '''
    Testing update doc function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user: BaseModel = User(email='user@user.com')
        user1 = (await user.add()).unwrap()

        updated_doc = {'email': 'user@example.com', 'phone': '0800-22-33-11'}

        doc = (await user1.update(doc=updated_doc)).unwrap()
        assert isinstance(doc, BaseModel)
        assert updated_doc['email'] == doc.email
        assert updated_doc['phone'] == doc.phone


@pytest.mark.asyncio
async def test_orm_decl_update_model(couch_setup):
    '''
    Testing update model function - when model is updated with additional fields
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    # add user ------------------------------------------------
    # note that in model-0.yaml User model does not have attribute 'age'!
    model_path = 'orm/decl/model_test_files/yaml/model-0.yaml'
    loader = CouchLoader(db, path=model_path)
    User = loader.User

    user_instantiated: BaseModel = User(email='user@user.com')
    user_added = (await user_instantiated.add()).unwrap()

    # get user --------------------------
    # model User is updated with attribute 'age' in model-0a.yaml
    model_path = 'orm/decl/model_test_files/yaml/model-0a.yaml'
    loader = CouchLoader(db, path=model_path)
    User = loader.User

    fetched_user: User = (await User.get(docid=user_added._id)).unwrap()

    # update user --------------------------
    update_doc = {'email': 'user@example.com', 'phone': '0800-22-33-11', 'age': 28}

    updated_user = (await fetched_user.update(doc=update_doc)).unwrap()
    assert isinstance(updated_user, BaseModel)
    assert update_doc['email'] == updated_user.email
    assert update_doc['phone'] == updated_user.phone
    assert update_doc['age'] == updated_user.age


@pytest.mark.asyncio
async def test_orm_decl_update_doc_ignore_wrong_attribute(couch_setup):
    '''
    Testing update doc function by given wrong attribute - expected KeyError
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User = loader.User

    user = User(email='user@user.com')
    user1 = (await user.add()).unwrap()

    updated_doc = {'email': 'user@example.com', 'wrong attribute': 'wrong value'}
    updated: User = (await user1.update(doc=updated_doc)).unwrap()
    assert updated.email == 'user@example.com'


#
# delete model
#
@pytest.mark.asyncio
async def test_orm_decl_delete_doc(couch_setup):
    '''
    Testing delete doc function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user = User(email='user@example.com')
        user1 = (await user.add()).unwrap()

        deleted_user = (await user1.delete()).unwrap()
        assert isinstance(deleted_user, BaseModel)
        assert deleted_user._deleted

        deleted_user_dict: dict = deleted_user.asdict(_strict=False)
        assert deleted_user_dict['_deleted']


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_delete_doc_not_present_in_db(couch_setup):
    '''
    Testing delete model instance not previously added to database - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User = loader.User
    user: BaseModel = User(email='user@example.com')

    (await user.delete()).unwrap()


#
# model find
#
@pytest.mark.asyncio
async def test_orm_decl_find(couch_setup):
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/yaml/model-13.yaml')
    User: type = loader.User
    Profile: type = loader.Profile

    # add user
    user: User = User(email='user0@example.com')
    added_user = (await User.add(user)).unwrap()
    assert isinstance(added_user, User)

    UserIndex: type = loader.UserIndex_email
    index_ = (await UserIndex.create()).unwrap()
    assert isinstance(index_, UserIndex)

    # find profiles
    # res: tuple[list[Profile], str, str] = (await Profile.find(selector={"email": 'user0@example.com'})).unwrap()
    res: tuple[list[Profile], str, str] = (await Profile.find(selector={})).unwrap()
    # USE OF FIND FUNCTION WITHOUT selector
    # "\\$collection" key will be implicitly added to selector with value = class name
    ## find users
    res: tuple[list[User], str, str] = (await User.find()).unwrap()
    # selector = {'\\$collection': 'User'})
    users, *_ = res
    assert users

    # USE OF FIND FUNCTION WITHOUT "$collection" KEY IN SELECTOR
    # "\\$collection" key will be implicitly added to selector with value = class name
    ## find users
    res: tuple[list[User], str, str] = (await User.find(selector={"email": 'user0@example.com'})).unwrap()
    # selector = {'email': 'user0@example.com', '\\$collection': 'User'})
    users, *_ = res
    assert users

    # USE OF FIND FUNCTION WITHOUT OR WITH ONE OR TWO ESCAPE CHARACTERS IN SELECTOR
    # whatever is value of "$collection", "\$collection" or "\\$collection" key will be overridden with class name
    ## find profiles
    res: tuple[list[Profile], str, str] = (await Profile.find(selector={
        "$collection": "WHATEVER IS THE VALUE HERE, WILL BE OVERRIDDEN WITH CLASS NAME",
        "email": 'user0@example.com'})).unwrap()
    # selector = {'email': 'user0@example.com', '\\$collection': 'Profile'}
    profiles, *_ = res
    assert not profiles

    res: tuple[list[Profile], str, str] = (await Profile.find(selector={
        "\$collection": "WHATEVER IS THE VALUE HERE, WILL BE OVERRIDDEN WITH CLASS NAME",
        "email": 'user0@example.com'})).unwrap()
    # selector: = {'email': 'user0@example.com', '\\$collection': 'Profile'}
    profiles, *_ = res
    assert not profiles

    # find users
    res: tuple[list[User], str, str] = (await User.find(selector={"email": 'user0@example.com'})).unwrap()
    # res: tuple[list[User], str, str] = (await User.find(selector={})).unwrap()
    users, bookmark, warning = res
    # $ find users
    res: tuple[list[User], str, str] = (await User.find(selector={
        "\\$collection": "WHATEVER IS THE VALUE HERE, WILL BE OVERRIDDEN WITH CLASS NAME",
        "email": 'user0@example.com'})).unwrap()
    # selector = {'email': 'user0@example.com', '\\$collection': 'User'}
    users, *_ = res
    assert users
    assert warning is None


#
# add attachment
#
@pytest.mark.asyncio
async def test_orm_decl_add_attachment(couch_setup):
    '''
    Testing add_attachment function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User: type = loader.User
        user: User

        user: BaseModel = User(email='user@user.com')
        user1 = (await user.add()).unwrap()

        file_name = 'abc.txt'

        with open(file_name, 'w') as f:
            f.write('This is a text')

        with open(file_name, 'rb') as reader:
            content = reader.read()

        data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user1.add_attachment(attachment_name=file_name, body=content)).unwrap()

        doc, att = data_tuple
        assert isinstance(doc, BaseModel)
        assert isinstance(att, CouchAttachment)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_add_attachment_wrong_id(couch_setup):
    '''
    Testing add attachment given the wrong id - expected ResultException
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User: type = loader.User

    user: User = User(email='user@user.com')

    user_created: User = (await user.add()).unwrap()

    file_name = 'abc.txt'
    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    user_created.entries['_id'] = user_created._id + 'f'

    (await user_created.add_attachment(attachment_name=file_name, body=content)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_add_attachment_missing_attachment_name_and_body(couch_setup):
    '''
    Testing add attachment missing attachment name and body - expected ResultException
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User: type = loader.User
    user: User = User(email='user@user.com')
    user_created: User = (await user.add()).unwrap()

    (await user_created.add_attachment(attachment_name='', body=None)).unwrap()


#
# get attachment
#
@pytest.mark.asyncio
async def test_orm_decl_get_attachment(couch_setup):
    '''
    Testing get_attachment function - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        user: BaseModel = User(email='user@user.com')
        user1 = (await user.add()).unwrap()

        file_name = 'abc.txt'

        with open(file_name, 'w') as f:
            f.write('This is a text')

        with open(file_name, 'rb') as reader:
            content = reader.read()

        user2, _ = (await user1.add_attachment(attachment_name=file_name, body=content)).unwrap()
        attachment: CouchAttachment = (await user2.get_attachment(attachment_name=file_name)).unwrap()
        assert isinstance(attachment, CouchAttachment)
        assert attachment['attachment_name'] == file_name


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_get_attachment_wrong_attn_name(couch_setup):
    '''
    Testing get attachment given wrong attachment name - expect ResultException
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')
    User: type = loader.User
    user: User
    user: BaseModel = User(email='user@user.com')

    user_created: User
    user_created = (await User.add(user)).unwrap()

    file_name = 'abc.txt'
    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    doc, _ = (await user_created.add_attachment(attachment_name=file_name, body=content)).unwrap()

    (await doc.get_attachment(attachment_name='file_name')).unwrap()


#
# update attachment
#
@pytest.mark.asyncio
async def test_orm_decl_update_attachment(couch_setup):
    '''
    Testing update_attachment - expected success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User: type = loader.User

        user: User = User(email='example@example.com')
        user_model: User = (await user.add()).unwrap()

        file_name = 'abc.txt'
        with open(file_name, 'w') as f:
            f.write('This is a text')

        with open(file_name, 'rb') as reader:
            content = reader.read()

        data_tuple: tuple[BaseModel, CouchAttachment] = (
            await user_model.add_attachment(attachment_name=file_name, body=content)).unwrap()
        doc, att = data_tuple

        file_name = 'abc.txt'
        with open(file_name, 'w') as f:
            f.write('This is a updated attachment text')

        with open(file_name, 'rb') as reader:
            content = reader.read()

        updated_data_tuple: tuple[BaseModel, CouchAttachment] = (
            await doc.update_attachment(attachment_name='New updated name', body=content)).unwrap()
        doc, att = updated_data_tuple
        assert isinstance(doc, BaseModel)
        assert isinstance(att, CouchAttachment)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_update_attachment_missing_attachment_name_and_body(couch_setup):
    '''
    Testing update attachment, missing attachment name and body - expected ResultException
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')

    User: type = loader.User
    user: User = User(email='example@example.com')
    user_model: User = (await user.add()).unwrap()

    file_name = 'abc.txt'
    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    data_tuple: tuple[BaseModel, CouchAttachment] = (
        await user_model.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc, att = data_tuple

    file_name = 'abc.txt'
    with open(file_name, 'w') as f:
        f.write('This is a updated attachment text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    (await doc.update_attachment(attachment_name='', body=None)).unwrap()


#
# remove attachment
#
@pytest.mark.asyncio
async def test_orm_decl_remove_attachment(couch_setup):
    '''
    Testing remove_attachment - expected success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        User: type = loader.User

        user: User
        user: BaseModel = User(email='example@example.com')
        user_model = (await user.add()).unwrap()

        file_name_1 = 'abc.txt'
        with open(file_name_1, 'w') as f:
            f.write('This is a text')

        with open(file_name_1, 'rb') as reader:
            content = reader.read()

        res_tuple = (await user_model.add_attachment(attachment_name=file_name_1, body=content)).unwrap()
        updated_model, attachment = res_tuple

        # Second attachment
        file_name_2 = 'bcd.txt'
        with open(file_name_2, 'w') as f:
            f.write('This is a second text')

        with open(file_name_2, 'rb') as reader:
            content = reader.read()

        model = (await updated_model.add_attachment(attachment_name=file_name_2, body=content)).unwrap()
        doc, att = model
        model_instance = (await doc.remove_attachment(attachment_name=file_name_2)).unwrap()
        assert isinstance(model_instance, BaseModel)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_remove_attachment_wrong_id(couch_setup):
    '''
    Testing remove attachment given wrong id - expected ResultException
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml')

    User: type = loader.User

    user: User
    user: BaseModel = User(email='example@example.com')
    user_model = (await user.add()).unwrap()

    file_name = 'abc.txt'
    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    model = (await user_model.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc, att = model
    doc.entries['_id'] = 'wrong id'

    (await doc.remove_attachment(attachment_name=file_name)).unwrap()


#
# # INDEX
#

#
# create index
#
@pytest.mark.asyncio
async def test_orm_decl_index_create(couch_setup):
    '''
    This function tests creating index - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        UserIndex: type = loader.UserIndex_email_usertype

        index_ = (await UserIndex.create()).unwrap()
        assert isinstance(index_, UserIndex)


@pytest.mark.asyncio
async def test_orm_decl_index_instantiate(couch_setup):
    '''
    This function tests instantiation of UserIndex by given all params - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        UserIndex: type = loader.UserIndex_email_usertype

        index1 = UserIndex(id='id', name='name', fields='fields', type='type')
        assert isinstance(index1, UserIndex)


#
# get index
#
@pytest.mark.asyncio
async def test_orm_decl_index_get(couch_setup):
    '''
    Test get indexes - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        UserIndex: type = loader.UserIndex_email_usertype
        (await UserIndex.create()).unwrap()

        UserIndex: type = loader.UserIndex_email
        (await UserIndex.create()).unwrap()

        index_list: list[BaseIndex] = (await UserIndex.get()).unwrap()
        assert isinstance(index_list, list)
        assert all(isinstance(index, BaseIndex) for index in index_list)
        assert len(index_list) == 2


#
# update index
#
@pytest.mark.asyncio
async def test_orm_decl_index_update(couch_setup):
    '''
    Testing update index function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        UserIndex: type = loader.UserIndex_email_usertype
        (await UserIndex.create(ddoc='ddoc')).unwrap()

        UserIndex.fields = ['email']
        updated_index = (await UserIndex.update(ddoc='ddoc')).unwrap()

        assert isinstance(updated_index.fields, list)

        found = False
        for element in updated_index.fields:
            if 'email' in element:
                found = True
                break
        assert found
        assert isinstance(updated_index, BaseIndex)


#
# delete index
#
@pytest.mark.asyncio
async def test_orm_decl_index_delete(couch_setup):
    '''
    Testing delete index function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        UserIndex: type = loader.UserIndex_email_usertype
        (await UserIndex.create(ddoc='ddoc')).unwrap()

        deleted_index: bool = (await UserIndex.delete(designdoc='ddoc')).unwrap()
        assert deleted_index


#
# # LOADER
#

@pytest.mark.asyncio
@pytest.mark.xfail(raises=AttributeError, strict=True)
async def test_orm_decl_loader_init_schema(couch_setup):
    '''
    Testing loader init schema - AttributeError expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    CouchLoader(db, schema={'x': '1'}).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=CouchLoaderError, strict=True)
async def test_orm_decl_loader_path_end_schema(couch_setup):
    '''
    Testing loader init schema - CouchLoaderError expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    CouchLoader(db, path='orm/decl/model_test_files/toml/model-0.toml', schema={'x': '1'}).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ValueError, strict=True)
async def test_orm_decl_loader_wrong_type(couch_setup):
    '''
    Loader creates dynamically Model by given wrong type - expected ValueError
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    CouchLoader(db, path='orm/decl/model_test_files/toml/model-1.toml')


#
# model/object/index
#
@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_copy_deepcopy(couch_setup):
    '''
    Test loader model object copy and deepcopy functions - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-3.toml',
        'orm/decl/model_test_files/yaml/model-3.yaml',
        'orm/decl/model_test_files/json/model-3.json',
        'orm/decl/model_test_files/json5/model-3.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        Profile: type = loader.Profile
        assert Profile

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        a0 = AgentProfile(x=10)
        assert isinstance(a0, AgentProfile)
        b0 = BrokerProfile(y=100.0)
        assert isinstance(b0, BrokerProfile)

        p0 = Profile(
            agent_profile=a0,
            broker_profile=b0,
            profile=a0,
            profiles=[a0, b0]
        )
        assert isinstance(p0, Profile)

        p1 = Profile(
            agent_profile=a0,
            broker_profile=b0,
            profile=b0,
            profiles=[a0, b0]
        )
        assert isinstance(p1, Profile)

        p0: Profile = (await Profile.add(p0)).unwrap()
        assert isinstance(p0, Profile)

        p1: Profile = (await Profile.add(p1)).unwrap()
        assert isinstance(p1, Profile)

        a0_0 = copy(a0)
        assert a0.asdict() == a0_0

        a0_1 = deepcopy(a0)
        assert a0_0 == a0_1
        assert a0.asdict() == a0_1

        p0_0 = copy(p0)
        assert p0.asdict() == p0_0

        p0_1 = deepcopy(p0)
        assert p0_0 == p0_1
        assert p0.asdict() == p0_1

        p1_0 = copy(p1)
        assert p1.asdict() == p1_0

        p1_1 = deepcopy(p1)
        assert p1_0 == p1_1
        assert p1.asdict() == p1_1


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_instances(couch_setup):
    '''
    Test loader model, object, create instances - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    models_paths = [
        'orm/decl/model_test_files/toml/model-3.toml',
        'orm/decl/model_test_files/yaml/model-3.yaml',
        'orm/decl/model_test_files/json/model-3.json',
        'orm/decl/model_test_files/json5/model-3.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        Profile: type = loader.Profile
        assert Profile

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        a0 = AgentProfile(x=10)
        b0 = BrokerProfile(y=100.0)

        p0 = Profile(
            agent_profile=a0,
            broker_profile=b0,
            profile=a0,
            profiles=[a0, b0],
        )
        assert isinstance(p0, Profile)

        p1 = Profile(
            agent_profile=a0,
            broker_profile=b0,
            profile=b0,
            profiles=[b0, a0],
        )
        assert isinstance(p1, Profile)

        p0: Profile = (await Profile.add(p0)).unwrap()
        assert isinstance(p0, Profile)

        p1: Profile = (await Profile.add(p1)).unwrap()
        assert isinstance(p1, Profile)


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_load_type(couch_setup):
    '''
    Test loader model, object, load type - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-4.toml',
        'orm/decl/model_test_files/yaml/model-4.yaml',
        'orm/decl/model_test_files/json/model-4.json',
        'orm/decl/model_test_files/json5/model-4.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_init_field_key_not_in_kwargs(couch_setup):
    '''
    Test loader init field key is not in passed kwargs from model-toml - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    models_paths = [
        'orm/decl/model_test_files/toml/model-3.toml',
        'orm/decl/model_test_files/yaml/model-3.yaml',
        'orm/decl/model_test_files/json/model-3.json',
        'orm/decl/model_test_files/json5/model-3.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        a0 = AgentProfile(y=10)
        assert isinstance(a0, AgentProfile)


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_init_field_key_callable(couch_setup):
    '''
    Test loader init field key is not in passed kwargs from model-toml - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    models_paths = [
        'orm/decl/model_test_files/toml/model-3.toml',
        'orm/decl/model_test_files/yaml/model-3.yaml',
        'orm/decl/model_test_files/json/model-3.json',
        'orm/decl/model_test_files/json5/model-3.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        a0 = AgentProfile(y=None)


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_dict_collection(couch_setup):
    '''
    Test loader model object given dict - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    models_paths = [
        'orm/decl/model_test_files/toml/model-5.toml',
        'orm/decl/model_test_files/yaml/model-5.yaml',
        'orm/decl/model_test_files/json/model-5.json',
        'orm/decl/model_test_files/json5/model-5.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        b0 = BrokerProfile(y=100.0)
        assert isinstance(b0, BrokerProfile)

        a0 = AgentProfile(x=10, broker_profile=b0.asdict())


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_list_collection(couch_setup):
    '''
    Test loader model object given list of profiles - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    models_paths = [
        'orm/decl/model_test_files/toml/model-6.toml',
        'orm/decl/model_test_files/yaml/model-6.yaml',
        'orm/decl/model_test_files/json/model-6.json',
        'orm/decl/model_test_files/json5/model-6.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        ClientProfile: type = loader.ClientProfile
        assert ClientProfile

        b0 = BrokerProfile(y=100.0)
        assert isinstance(b0, BrokerProfile)

        c0 = ClientProfile(z=1000.0)
        assert isinstance(c0, ClientProfile)

        a0 = AgentProfile(x=10, profiles=[b0, c0])
        assert isinstance(a0, AgentProfile)
        assert isinstance(a0.profiles, list)
        profiles_list: list = a0.profiles
        assert all(isinstance(e, BrokerProfile | ClientProfile) for e in profiles_list)


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_get_item(couch_setup):
    '''
    Test loader model object __getitem__ function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    models_paths = [
        'orm/decl/model_test_files/toml/model-6.toml',
        'orm/decl/model_test_files/yaml/model-6.yaml',
        'orm/decl/model_test_files/json/model-6.json',
        'orm/decl/model_test_files/json5/model-6.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        a0 = AgentProfile(x=10)
        x = a0.__getitem__('x')
        assert x


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_object_validate(couch_setup):
    ''' 
    Test loader model object validate function expecting error - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-6.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    a0 = AgentProfile(x='10')


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_object_validate_wrong_field_type(couch_setup):
    ''' 
    Test loader model object validate function expecting error given wrong field type - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-6.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    a0 = AgentProfile(x=10, profiles=[1])


@pytest.mark.asyncio
@pytest.mark.xfail(raises=FileConfigError, strict=True)
async def test_orm_decl_loader_path_not_found(couch_setup):
    ''' 
    Test loader path not found - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    loader = CouchLoader(db, path='path_not_found/abc.txt')


@pytest.mark.asyncio
@pytest.mark.xfail(raises=CouchLoaderError, strict=True)
async def test_orm_decl_loader_wrong_file_extension(couch_setup):
    ''' 
    Test loader wrong file extension - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    loader = CouchLoader(db, path='orm/decl/model_test_files/unsupported/model-6.txt')


@pytest.mark.asyncio
@pytest.mark.xfail(raises=JSONDecodeError, strict=True)
async def test_orm_decl_loader_invalid_json_format(couch_setup):
    ''' 
    Test loader, json syntax error - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    loader = CouchLoader(db, path='orm/decl/model_test_files/json/model-error.json')


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ValueError, strict=True)
async def test_orm_decl_loader_invalid_json5_format(couch_setup):
    ''' 
    Test loader, json5 syntax error - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    loader = CouchLoader(db, path='orm/decl/model_test_files/json5/model-error.json5')


@pytest.mark.asyncio
@pytest.mark.xfail(raises=TomlDecodeError, strict=True)
async def test_orm_decl_loader_invalid_toml_format(couch_setup):
    ''' 
    Test loader, toml syntax error - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-error.toml')


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ParserError, strict=True)
async def test_orm_decl_loader_invalid_yaml_format(couch_setup):
    ''' 
    Test loader, yaml syntax error - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    loader = CouchLoader(db, path='orm/decl/model_test_files/yaml/model-error.yaml')


@pytest.mark.asyncio
async def test_orm_decl_loader_generic_model_object_0(couch_setup):
    '''
    Testing instantiation of generic models and objects 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-7.toml',
        'orm/decl/model_test_files/yaml/model-7.yaml',
        'orm/decl/model_test_files/json/model-7.json',
        'orm/decl/model_test_files/json5/model-7.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        ClientProfile: type = loader.ClientProfile
        assert ClientProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        Profile: type = loader.Profile
        assert Profile

        a0 = AgentProfile[int](x=10)
        assert isinstance(a0, AgentProfile)

        c0 = ClientProfile[int](y=10)
        assert isinstance(c0, ClientProfile)

        b0 = BrokerProfile[int, float](y=100, z=200.0)
        assert isinstance(b0, BrokerProfile)

        p0 = Profile[int, float](
            agent_profile=a0,
            broker_profile=b0,
            profile=a0,
            profiles=[a0, b0],
            a_items=[1],
            b_items={1: 1.0, 2: 2.0},
            c_items=(1, 2.0),
            d_items=(1, 2.0, 3, 4.0),
        )
        assert isinstance(p0, Profile)

        p1 = Profile[AgentProfile[int], BrokerProfile[int, float]](
            agent_profile=a0,
            broker_profile=b0,
            profile=a0,
            profiles=[a0, b0],
            a_items=[a0],
            b_items={a0: b0},
            c_items=(a0, b0),
            d_items=(a0, b0, a0, b0),
        )
        assert isinstance(p1, Profile)


@pytest.mark.asyncio
async def test_orm_decl_loader_generic_model_fields_start_with_special_character(couch_setup):
    '''
    Testing instantiating of model using special character as field - expected success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-8.toml',
        'orm/decl/model_test_files/yaml/model-8.yaml',
        'orm/decl/model_test_files/json/model-8.json',
        'orm/decl/model_test_files/json5/model-8.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        ClientProfile: type = loader.ClientProfile
        assert ClientProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        Profile: type = loader.Profile
        assert Profile

        a0 = AgentProfile[int](x=10)
        assert isinstance(a0, AgentProfile)

        c0 = ClientProfile[int](y=10)
        assert isinstance(c0, ClientProfile)

        b0 = BrokerProfile[int, float](y=100, z=200.0)
        assert isinstance(b0, BrokerProfile)

        p0 = Profile[int, float](
            agent_profile=a0,
            broker_profile=b0,
            profile=a0,
            profiles=[a0, b0],
            a_items=[1],
            b_items={1: 1.0, 2: 2.0},
            c_items=(1, 2.0),
            d_items=(1, 2.0, 3, 4.0),
        )
        assert isinstance(p0, Profile)

        p1 = Profile[AgentProfile[int], BrokerProfile[int, float]](
            agent_profile=a0,
            broker_profile=b0,
            profile=a0,
            profiles=[a0, b0],
            a_items=[a0],
            b_items={a0: b0},
            c_items=(a0, b0),
            d_items=(a0, b0, a0, b0),
        )
        assert isinstance(p1, Profile)


@pytest.mark.asyncio
async def test_orm_decl_loader_model_spec_types_not_tuple(couch_setup):
    '''
    Testing instantiating for model using spec types different type thats not tuple - expecting success 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-9.toml',
        'orm/decl/model_test_files/yaml/model-9.yaml',
        'orm/decl/model_test_files/json/model-9.json',
        'orm/decl/model_test_files/json5/model-9.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        ClientProfile: type = loader.ClientProfile
        assert ClientProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        Profile: type = loader.Profile
        assert Profile

        a0 = AgentProfile[int](x=10)
        assert isinstance(a0, AgentProfile)

        c0 = ClientProfile[int](y=10)
        assert isinstance(c0, ClientProfile)

        b0 = BrokerProfile[int, float](y=100, z=200.0)
        assert isinstance(b0, BrokerProfile)

        p0 = Profile[int](
            agent_profile=a0,
            broker_profile=b0,
            profile=a0,
            profiles=[a0, b0],
            a_items=[1],
            b_items={1: b0, 2: b0},
            c_items=(1, b0),
            d_items=(1, b0, 3, b0),
        )
        assert isinstance(p0, Profile)


# FIXME:
@pytest.mark.asyncio
async def test_orm_decl_loader_object_redundant_float_type(couch_setup):
    '''
    Testing instantiating for object using redundant float type which is ignored - expecting success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-10.toml',
        'orm/decl/model_test_files/yaml/model-10.yaml',
        'orm/decl/model_test_files/json/model-10.json',
        'orm/decl/model_test_files/json5/model-10.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        AgentProfile: type = loader.AgentProfile
        assert AgentProfile

        BrokerProfile: type = loader.BrokerProfile
        assert BrokerProfile

        # Instantiate AgentProfile with redundant float type which is ignored
        a0 = AgentProfile[int, float](x=10, s=100.0)
        assert isinstance(a0, AgentProfile)

        b0 = BrokerProfile[int, float](y=100, z=200.0)
        assert isinstance(b0, BrokerProfile)


# @pytest.mark.asyncio
# @pytest.mark.xfail(raises = ValueError, strict = True)
# async def test_orm_decl_loader_object_wrong_value_for_object_field(couch_setup):
#     '''
#     Testing instantiating for object given wrong value for field  - expecting exception
#     '''
#     COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
#     client = CouchClient(COUCH_URI)
#     db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
#
#     loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-11.toml')
#
#     AgentProfile: type = loader.AgentProfile
#     assert AgentProfile
#
#     ClientProfile: type = loader.ClientProfile
#     assert ClientProfile
#
#     BrokerProfile: type = loader.BrokerProfile
#     assert BrokerProfile
#
#     a0 = AgentProfile[int](x = 10)
#     assert isinstance(a0, AgentProfile)
#
#     c0 = ClientProfile[int](y = 10)
#     assert isinstance(c0, ClientProfile)
#
#     # Attempt to instantiate BrokerProfile with wrong agent_profile param
#     b0 = BrokerProfile[int, float](
#         y = 100,
#         z = 200.0,
#         # wrong agent_profile param (passed instance of client_profile)
#         agent_profile = c0,
#         client_profile = c0)
#     assert isinstance(b0, BrokerProfile)


# @pytest.mark.asyncio
# @pytest.mark.xfail(raises = ValueError, strict=True)
# async def test_orm_decl_loader_model_object_wrong_object_type(couch_setup):
#     '''
#     Testing instantiating for object by given wrong object - expecting exception
#     '''
#     COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
#     client = CouchClient(COUCH_URI)
#     db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
#
#     loader = CouchLoader(db, path = 'orm/decl/model_test_files/toml/model-11.toml')
#
#     InternProfile: type = loader.InternProfile
#     assert InternProfile
#
#     ClientProfile: type = loader.ClientProfile
#     assert ClientProfile
#
#     PatientProfile: type = loader.PatientProfile
#     assert PatientProfile
#
#     i0 = InternProfile(x = 10)
#     assert isinstance(i0, InternProfile)
#
#     c0 = ClientProfile[int](y = 10)
#     assert isinstance(c0, ClientProfile)
#
#     p = PatientProfile(client_profile = i0)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_tuple_len_more_then_expected(couch_setup):
    '''
    Testing instantiating for model by given length more than expected - expecting exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-9.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    ClientProfile: type = loader.ClientProfile
    assert ClientProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    c0 = ClientProfile[int](y=10)
    assert isinstance(c0, ClientProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[int](
        agent_profile=a0,
        broker_profile=b0,
        profile=a0,
        profiles=[a0, b0],
        a_items=[1],
        b_items={1: b0, 2: b0},
        c_items=(1, 'test-str', b0),
        d_items=(1, b0, 3, b0),
    )
    assert isinstance(p0, Profile)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_tuple_not_all_generic_types_are_as_expected(couch_setup):
    '''
    Testing instantiating for model by given not all generic types as expected - expecting exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-9.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    ClientProfile: type = loader.ClientProfile
    assert ClientProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    c0 = ClientProfile[int](y=10)
    assert isinstance(c0, ClientProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[int](
        agent_profile=a0,
        broker_profile=b0,
        profile=a0,
        profiles=[a0, b0],
        a_items=[1],
        b_items={1: b0, 2: b0},
        c_items=(1.1, b0),
        d_items=(1, b0, 3, b0),
    )
    assert isinstance(p0, Profile)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_tuple_not_passed_union(couch_setup):
    '''
    Testing instantiating for model spec types tuple not passed union, expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-12.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    ClientProfile: type = loader.ClientProfile
    assert ClientProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    c0 = ClientProfile[int](y=10)
    assert isinstance(c0, ClientProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[int](
        agent_profile=a0,
        broker_profile=b0,
        profile=a0,
        profiles=[a0, b0],
        a_items=[1],
        b_items={1: b0, 2: b0},
        c_items=(1, b0),
        d_items=(1)
    )
    assert isinstance(p0, Profile)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_list_wrong_value_for_field(couch_setup):
    '''
    Testing initialization of object type by given wrong value for field - Expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-12.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    ClientProfile: type = loader.ClientProfile
    assert ClientProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    c0 = ClientProfile[int](y=10)
    assert isinstance(c0, ClientProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[int](
        agent_profile=a0,
        broker_profile=b0,
        profile=a0,
        profiles=(a0, b0),
        a_items=[1],
        b_items={1: b0, 2: b0},
        c_items=(1, b0),
        d_items=(1)
    )
    assert isinstance(p0, Profile)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_list_wrong_key_type_for_dict(couch_setup):
    '''
    Testing instantiating for model, spec types list, wrong key type for dict, expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-12.toml')

    Employee: type = loader.Employee
    assert Employee

    Delivery: type = loader.Delivery
    assert Delivery

    d = Delivery()
    e = Employee[int](job={'1': d})


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_unknown_field_type(couch_setup):
    '''
    Testing unknown field type - expecting success 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-12.toml')

    Employee: type = loader.Employee
    assert Employee

    Delivery: type = loader.Delivery
    assert Delivery

    d = Delivery()
    e = Employee[int](job_2={'1': d})


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_unknown_field_type_not_union(couch_setup):
    '''
    Testing unknown field type not union
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-12.toml')

    Employee: type = loader.Employee
    assert Employee

    Delivery: type = loader.Delivery
    assert Delivery

    d = Delivery()
    e = Employee[int](job_3=12.1)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_field_type_unknown_instance_set_provided(couch_setup):
    '''
    Testing unknown instance set provided
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-12.toml')

    Employee: type = loader.Employee
    assert Employee

    e = Employee[int](job_4=set())


# @pytest.mark.asyncio
# @pytest.mark.xfail(raises = ValueError, strict = True)
# async def test_orm_decl_loader_model_object_union_wrong_type_object(couch_setup):
#     '''
#     Testing instantiating model when wrong type containing object passed - expecting exception
#     '''
#     COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
#     client = CouchClient(COUCH_URI)
#     db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
#
#     loader = CouchLoader(db, path = 'orm/decl/model_test_files/toml/model-13.toml')
#
#     AgentProfile: type = loader.AgentProfile
#     assert AgentProfile
#
#     BrokerProfile: type = loader.BrokerProfile
#     assert BrokerProfile
#
#     ClientProfile: type = loader.ClientProfile
#     assert ClientProfile
#
#     Profile: type = loader.Profile
#     assert Profile
#
#     a0 = AgentProfile[int](x = 10)
#     assert isinstance(a0, AgentProfile)
#
#     b0 = BrokerProfile[int, float](y = 100, z = 200.0)
#     assert isinstance(b0, BrokerProfile)
#
#     c0 = ClientProfile[int](y = 10)
#     assert isinstance(c0, ClientProfile)
#
#     # Attempt to instantiate Profile with wrong typa 'profile' param passed
#     # (ClientProfile instead of expected AgentProfile | BrokerProfile)
#     p0 = Profile(profile = c0)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_list_wrong_generic_type_as_item_in_list(couch_setup):
    '''
    Testing initialization of model type by given wrong generic type as item in list - Expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-14.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    ClientProfile: type = loader.ClientProfile
    assert ClientProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    c0 = ClientProfile[int](y=10)
    assert isinstance(c0, ClientProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[AgentProfile[int]](a_items=[b0])
    assert isinstance(p0, Profile)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_dict_wrong_generic_type_as_item_in_dict(couch_setup):
    '''
    Testing initialization of model type by given wrong generic type as item in dict - Expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-15.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[int](
        a_items={1: b0},
    )
    assert isinstance(p0, Profile)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_tuple_wrong_generic_type_as_item_in_tuple(couch_setup):
    '''
    Testing initialization of model type by given wrong generic type as item in tuple - Expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-16.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[AgentProfile](a_items=(b0, 1))
    assert isinstance(p0, Profile)


@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_decl_loader_model_spec_types_tuple_wrong_union_type(couch_setup):
    '''
    Testing initialization of model by given wrong union type as item in tuple - Expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    loader = CouchLoader(db, path='orm/decl/model_test_files/toml/model-17.toml')

    AgentProfile: type = loader.AgentProfile
    assert AgentProfile

    BrokerProfile: type = loader.BrokerProfile
    assert BrokerProfile

    Profile: type = loader.Profile
    assert Profile

    a0 = AgentProfile[int](x=10)
    assert isinstance(a0, AgentProfile)

    b0 = BrokerProfile[int, float](y=100, z=200.0)
    assert isinstance(b0, BrokerProfile)

    p0 = Profile[int](a_items=(b0,))
    assert isinstance(p0, Profile)

    p1 = Profile[int](a_items=(a0, 1))


#
# get document with attachment metadata
#
@pytest.mark.asyncio
async def test_orm_decl_get_document_with_attachment_metadata(couch_setup):
    '''
    Testing get_document function - expect document with attachment metadata
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-0.toml',
        'orm/decl/model_test_files/yaml/model-0.yaml',
        'orm/decl/model_test_files/json/model-0.json',
        'orm/decl/model_test_files/json5/model-0.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        User = loader.User

        # Create user and add to db
        user: BaseModel = User(email='user@user.com')
        user1 = (await user.add()).unwrap()

        # Create file and add to document
        file_name = 'abc.txt'

        with open(file_name, 'w') as f:
            f.write('This is a text')

        with open(file_name, 'rb') as reader:
            content = reader.read()

        user2, _ = (await user1.add_attachment(attachment_name=file_name, body=content)).unwrap()

        # Fetch attachment by name
        attachment: CouchAttachment = (await user2.get_attachment(attachment_name=file_name)).unwrap()
        assert isinstance(attachment, CouchAttachment)
        assert attachment['attachment_name'] == file_name

        # Fetch document with attachment metadata
        user3 = (await User.get(docid=user._id)).unwrap()
        assert user3._attachments


@pytest.mark.asyncio
async def test_orm_decl_loader_model_object_load_type(couch_setup):
    '''
    Test loader model, object, load type - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/toml/model-4.toml',
        'orm/decl/model_test_files/yaml/model-4.yaml',
        'orm/decl/model_test_files/json/model-4.json',
        'orm/decl/model_test_files/json5/model-4.json5',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        model_name = 'User'

        model: type = loader[model_name]
        assert model.__name__ == model_name


@pytest.mark.asyncio
async def test_orm_decl_loader_yaml_include_model_object_load_type(couch_setup):
    '''
    Test yaml "include" loader
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/yaml/model-11.yaml',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        model_name = 'User'

        model: type = loader[model_name]
        assert model.__name__ == model_name


@pytest.mark.asyncio
async def test_orm_decl_loader_yaml_include_recursive_model_object_load_type(couch_setup):
    '''
    Test yaml recursive "include" loader
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/yaml/model-12.yaml',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)

        model_name: str = 'User'
        model: type = loader[model_name]
        assert model.__name__ == model_name

        model_name: str = 'Profile'
        model: type = loader[model_name]
        assert model.__name__ == model_name


@pytest.mark.asyncio
async def test_orm_decl_loader_float_accept_int(couch_setup):
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database(COUCH_DATABASE).unwrap()

    models_paths = [
        'orm/decl/model_test_files/yaml/model-0.yaml',
    ]

    for model_path in models_paths:
        loader = CouchLoader(db, path=model_path)
        Point: type = loader.Point

        point_instance = Point(x=1, y=5)
        point_created = (await point_instance.add()).unwrap()

        assert isinstance(point_created, BaseModel)

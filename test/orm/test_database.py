import pytest
import json

from thresult import ResultException

from thcouch.core.db import head_db
from thcouch.orm import CouchDocument, CouchIndex, CouchDatabase, CouchClient, CouchAttachment

@pytest.fixture(scope='module')
def test_doc_list():
    '''
    This function is for creating the doc list for testing purposes
    '''
    test_doc_list: list[dict] = [
        {'x': 10, 'y': 10, 'name': 'John', 'email': 'john@example.com'},
        {'x': 10, 'y': 15, 'name': 'Steve', 'email': 'steve@example.com'},
        {'x': 10, 'y': 20, 'name': 'Michael', 'email': 'michael@example.com'},
        {'x': 10, 'y': 25, 'name': 'Peter', 'email': 'peter@example.com'},
        {'x': 10, 'y': 30, 'name': 'John', 'email': 'john01@example.com'},
    ]

    yield test_doc_list


@pytest.mark.asyncio
async def test_orm_database_create_existing_db(couch_setup):
    '''
    This function tests create existing db - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)

    db_to_create = COUCH_DATABASE

    # assert that db already exists
    assert (await head_db(uri=COUCH_URI, db=db_to_create)).unwrap().exists is True

    # create function returns existing db
    db: CouchDatabase = client.database(db_to_create).unwrap()
    db_: CouchDatabase = (await db.create()).unwrap()

    assert isinstance(db_, CouchDatabase)
    assert db_.db == db_to_create


@pytest.mark.asyncio
async def test_orm_database_create_non_existing_db(couch_setup):
    '''
    This function tests create non existing db - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)

    db_to_create = 'test_db'

    # assert db_to_create not already exists
    assert (await head_db(uri=COUCH_URI, db=db_to_create)).unwrap().exists is False

    # create db
    db: CouchDatabase = client.database(db_to_create).unwrap()
    db_: CouchDatabase = (await db.create()).unwrap()

    assert isinstance(db_, CouchDatabase)
    assert (await head_db(uri=COUCH_URI, db=db_to_create)).unwrap().exists is True
    assert db_.db == db_to_create


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_database_create_fail_non_existing_db_and_false_param(couch_setup):
    '''
    This function tests create non existing db, - expected exception while given invalid format db name
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)

    db_to_create = 'existing_db'

    # assert db_to_create not already exists
    assert (await head_db(uri=COUCH_URI, db=db_to_create)).unwrap().exists is False

    # create db with if_not_exists param False
    db: CouchDatabase = client.database(db_to_create).unwrap()
    db_: CouchDatabase = (await db.create(if_not_exists=False)).unwrap()


#
# delete database
#
@pytest.mark.asyncio
async def test_orm_database_delete_ok(couch_setup):
    '''
    This function tests delete db - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db = client.database('test_delete_db').unwrap()

    # create database
    created_db: CouchDatabase = (await db.create()).unwrap()
    assert isinstance(created_db, CouchDatabase)
    assert (await head_db(uri=COUCH_URI, db='test_delete_db')).unwrap().exists is True

    # delete database
    res: CouchDatabase = (await created_db.delete()).unwrap()
    assert isinstance(res, CouchDatabase)
    assert (await head_db(uri=COUCH_URI, db='test_delete_db')).unwrap().exists is False


#
# add document
#
@pytest.mark.asyncio
async def test_orm_database_add_document_ok(couch_setup):
    '''
    This function tests add_document function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Document to create
    doc: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc)).unwrap()
    db, created_doc = doc_tuple
 
    assert isinstance(db, CouchDatabase)
    assert isinstance(created_doc, CouchDocument)
    assert created_doc['x'] == doc['x']


@pytest.mark.asyncio
async def test_orm_database_add_document_ok_update_document(couch_setup):
    '''
    This function tests add_document function updating the document - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create test doc
    doc: dict = {'x': 10}
    doc_tuple1: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc)).unwrap()
    _, doc1 = doc_tuple1

    # Update test doc
    doc1['x'] = 15
    doc_tuple2: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc1)).unwrap()
    db, updated_doc = doc_tuple2

    assert isinstance(db, CouchDatabase)
    assert isinstance(updated_doc, CouchDocument)
    assert updated_doc['x'] != doc['x']
    assert updated_doc['x'] == doc1['x']
    assert updated_doc['_id'] == doc1['_id']
    assert updated_doc['_rev'] != doc1['_rev']


#
# get document
#
@pytest.mark.asyncio
async def test_orm_database_get_document_ok(couch_setup):
    '''
    Returns document by the specified docid from the specified db. - success
    Unless you request a specific revision, the latest revision of the document will always be returned.
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Get doc
    doc_id = created_doc['_id']
    doc: CouchDocument = (await db.get_document(doc_id)).unwrap()

    assert isinstance(doc, CouchDocument)
    assert doc['_id'] == created_doc['_id']
    assert doc['_rev'] == created_doc['_rev']


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_database_get_document_fails_non_existing_document(couch_setup):
    '''
    This function tests get document - expected exception while given non-existing document id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc: CouchDocument = (await db.get_document(docid='doc_id')).unwrap()


# FIXME: ContentTypeError, message:'Attempt to decode JSON with unexpected mimetype: multipart/related';
#  'Content-Type': 'multipart/related;
@pytest.mark.skip(reason='Call to couchdb raises ContentTypeError when "attachments" param is set to true!')
@pytest.mark.asyncio
async def test_orm_database_get_document_ok_attachments_true(couch_setup):
    '''
    This function tests get document with attachments param set to true -
    Includes attachments bodies in response. Default is false
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_dict: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc_dict)).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Add attachment 1
    file_name = 'abc.txt'
    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    attachment_1_added: tuple[CouchDocument, CouchAttachment] = (
        await created_doc.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc_1, att_1 = attachment_1_added

    # Add attachment 2
    file_name = 'def.txt'
    with open(file_name, 'w') as f:
        f.write('This is a another text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    attachment_2_added: tuple[CouchDocument, CouchAttachment] = (
        await doc_1.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc2, att_2 = attachment_2_added

    # Get doc with specific id - retrieves document with all attachment bodies
    doc_id = doc2['_id']
    get_doc_: CouchDocument = (await db.get_document(docid=doc_id, attachments=True)).unwrap()

    assert isinstance(get_doc_, CouchDocument)


# FIXME: Line 294 - db.get_document(docid=doc_id, att_encoding_info=True) - att_encoding_info=True does not produce any effect
@pytest.mark.asyncio
async def test_orm_database_get_document_ok_att_encoding_info_true(couch_setup):
    '''
    This function tests get document with att_encoding_info param set to true -
    Includes encoding information in attachment stubs
    if the particular attachment is compressed. Default is false
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_dict: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc_dict)).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # Add attachment 1
    attachment_1_added: tuple[CouchDocument, CouchAttachment] = (
        await created_doc.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc_1, att_1 = attachment_1_added

    file_name = 'def.txt'

    with open(file_name, 'w') as f:
        f.write('This is a another text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # Add attachment 2
    attachment_2_added: tuple[CouchDocument, CouchAttachment] = (
        await doc_1.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc2, att_2 = attachment_2_added

    # Get doc with specific id - retrieves document with all attachment bodies
    doc_id = doc2['_id']
    get_doc_: CouchDocument = (await db.get_document(docid=doc_id, att_encoding_info=True)).unwrap()
   
    assert isinstance(get_doc_, CouchDocument)


# FIXME:  Line 348 - (await db.get_document(docid=doc_id, atts_since=[rev])).unwrap()
#  ContentTypeError, message:'Attempt to decode JSON with unexpected mimetype: multipart/related';
@pytest.mark.skip(reason='Not possible to test - couchdb returns ContentTypeError error!')
@pytest.mark.asyncio
async def test_orm_database_get_document_ok_atts_since_true(couch_setup):
    '''
    This function tests get document with atts_since param set to true -
    Includes attachments only since specified revisions.
    Doesn’t includes attachments for specified revisions.
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_dict: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc_dict)).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # Add attachment 1
    attachment_1_added: tuple[CouchDocument, CouchAttachment] = (
        await created_doc.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc_1, att_1 = attachment_1_added

    file_name = 'def.txt'

    with open(file_name, 'w') as f:
        f.write('This is a another text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # Add attachment 2
    attachment_2_added: tuple[CouchDocument, CouchAttachment] = (
        await doc_1.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc2, att_2 = attachment_2_added

    # Get doc with specific id - retrieves document with all attachment bodies
    doc_id = doc2['_id']
    rev = doc_1['_rev']
    get_doc_: CouchDocument = (await db.get_document(docid=doc_id, atts_since=[rev])).unwrap()

    assert isinstance(get_doc_, CouchDocument)


@pytest.mark.asyncio
async def test_orm_database_get_document_ok_latest_param_true(couch_setup):
    '''
    This function tests get document with latest param set to true - success
    Forces retrieving latest “leaf” revision, no matter what rev was requested. Default is false
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_dict: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc_dict)).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Update doc
    created_doc['x'] = 25
    updated_doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=created_doc)).unwrap()
    _, updated_doc = updated_doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Get doc with not latest revision and latest param set to true
    doc_id = updated_doc['_id']
    not_latest_rev = created_doc['_rev']
    get_doc_: CouchDocument = (await db.get_document(docid=doc_id, rev=not_latest_rev, latest=True)).unwrap()

    assert isinstance(get_doc_, CouchDocument)
    assert get_doc_['_rev'] == updated_doc['_rev']


@pytest.mark.asyncio
async def test_orm_database_get_document_ok_local_seq_true(couch_setup):
    '''
    This function tests get document with local_seq param set to true - success
    Includes last update sequence for the document. Default is false.
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_dict: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc_dict)).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Update doc
    created_doc['x'] = 25
    updated_doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=created_doc)).unwrap()
    _, updated_doc = updated_doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Get doc with specific revision - retrieves document of specified revision
    doc_id = updated_doc['_id']
    get_doc_: CouchDocument = (await db.get_document(docid=doc_id, local_seq=True)).unwrap()

    assert isinstance(get_doc_, CouchDocument)
    assert get_doc_['_local_seq']


@pytest.mark.asyncio
async def test_orm_database_get_document_ok_specified_revision(couch_setup):
    '''
    This function tests get document of specified revision - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_dict: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc_dict)).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Update doc
    created_doc['x'] = 25
    updated_doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=created_doc)).unwrap()
    _, updated_doc = updated_doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Get doc with specific revision - retrieves document of specified revision
    doc_id = updated_doc['_id']
    not_latest_doc_rev = created_doc['_rev']
    get_doc_: CouchDocument = (await db.get_document(docid=doc_id, rev=not_latest_doc_rev)).unwrap()

    assert isinstance(get_doc_, CouchDocument)
    assert get_doc_['_rev'] == created_doc['_rev']


@pytest.mark.asyncio
async def test_orm_database_get_document_ok_revs_param_true(couch_setup):
    '''
    This function tests get document with revs param param set to true - success
    Includes list of all known document revisions
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # Create doc
    doc_dict: dict = {'x': 10}
    doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=doc_dict)).unwrap()
    _, created_doc = doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Update doc
    created_doc['x'] = 25
    updated_doc_tuple: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc=created_doc)).unwrap()
    _, updated_doc = updated_doc_tuple
    assert isinstance(created_doc, CouchDocument)

    # Get doc with specific revision - retrieves document of specified revision
    doc_id = updated_doc['_id']
    get_doc_: CouchDocument = (await db.get_document(docid=doc_id, revs=True)).unwrap()

    assert isinstance(get_doc_, CouchDocument)
    assert get_doc_['_revisions']


#
# all_docs
#
@pytest.mark.asyncio
async def test_orm_database_all_docs_ok(couch_setup, test_doc_list):
    '''
    This function tests all_docs function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # create docs for testing the get_all_docs function
    bulk_docs: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(test_doc_list)).unwrap()
    assert bulk_docs

    all_docs: list[CouchDocument] = (await db.all_docs()).unwrap()
    assert len(all_docs) == len(test_doc_list)


#
# bulk_get
#
@pytest.mark.asyncio
async def test_orm_database_bulk_get_ok(couch_setup, test_doc_list):
    '''
    This function tests bulk_get - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # create docs for testing
    bulk_docs: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(test_doc_list)).unwrap()
    _, docs = bulk_docs

    bulk_get_: list[dict] = (await db.bulk_get(docs=docs)).unwrap()
    assert len(bulk_get_) == len(docs)


@pytest.mark.asyncio
async def test_orm_database_bulk_get_ok_revs_true(couch_setup, test_doc_list):
    '''
    This function tests bulk_get with revs param set to true - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # create docs for testing
    bulk_docs: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(test_doc_list)).unwrap()
    _, docs = bulk_docs

    bulk_get_: list[dict] = (await db.bulk_get(docs=docs, revs=True)).unwrap()
    assert len(bulk_get_) == len(docs)
    assert '_revisions' in json.dumps(bulk_get_)


@pytest.mark.asyncio
async def test_orm_database_bulk_get_ok_non_existing_docs(couch_setup):
    '''
    This function tests bulk_get - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    docs: list[dict] = [{'id': 'x'}, {'id': 'y'}]

    bulk_get_: list[dict] = (await db.bulk_get(docs=docs)).unwrap()

    assert len(bulk_get_) == len(docs)
    assert 'error' in json.dumps(bulk_get_)
    assert json.dumps(bulk_get_).count('error') == len(docs) * 2
    doc_list: list[dict] = []
    for item in bulk_get_:
        doc_list.append(item['docs'][0])
    for item in doc_list:
        assert item['error']['error'] == 'not_found'


#
# bulk_docs
#
@pytest.mark.asyncio
async def test_orm_database_bulk_docs_ok_create_documents(couch_setup):
    '''
    This function tests create multiple documents with bulk_docs - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = CouchDatabase(client, COUCH_DATABASE)

    docs: list[dict] = [
        {'name': 'John', 'email': 'john@example.com'},
        {'name': 'Steve', 'email': 'steve@example.com'},
    ]

    bulk_docs_: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(docs)).unwrap()
    _, docs_array = bulk_docs_
    assert len(docs_array) == len(docs)


@pytest.mark.asyncio
async def test_orm_database_bulk_docs_ok_update_documents(couch_setup):
    '''
    This function tests update multiple documents with bulk_docs - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = CouchDatabase(client, COUCH_DATABASE)

    docs = [
        {'name': 'John', 'email': 'john@example.com'},
        {'name': 'Steve', 'email': 'steve@example.com'},
    ]

    created_docs_: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(docs)).unwrap()
    _, doc_array = created_docs_
    doc_1, doc_2 = doc_array

    update_docs_: list[dict] = [
        {'_id': doc_1['id'], '_rev': doc_1['rev'], 'name': 'John', 'email': 'peter@peter.com'},
        {'_id': doc_2['id'], '_rev': doc_2['rev'], 'name': 'Steve', 'email': 'steve@steve.com'},
    ]

    update_docs_: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(update_docs_)).unwrap()
    _, update_docs_array = update_docs_
    updated_doc_1, updated_doc_2 = update_docs_array

    assert doc_1['id'] == updated_doc_1['id'] and doc_2['id'] == updated_doc_2['id']
    assert doc_1['rev'] != updated_doc_1['rev'] and doc_2['rev'] != updated_doc_2['rev']


@pytest.mark.asyncio
@pytest.mark.skip(reason="Test is failing, need to lookup at CouchDB documentation about passing new_edits parameter")
async def test_orm_database_bulk_docs_ok_update_documents_new_edits_param_false(couch_setup):
    '''
    This function tests updating multiple documents with bulk_docs function
    when new_edits param is set to False - prevents new revs assigning
    Reference:
    https://docs.couchdb.org/en/stable/api/database/bulk-api.html#db-bulk-docs
    https://docs.couchdb.org/en/stable/replication/protocol.html?highlight=2.4.2.5.2#upload-batch-of-changed-documents
    https://github.com/apache/couchdb-documentation/issues/390
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = CouchDatabase(client, COUCH_DATABASE)

    docs = [
        {'name': 'John', 'email': 'john@example.com'},
        {'name': 'Steve', 'email': 'steve@example.com'},
    ]

    # Create test documents in db
    created_docs_: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(docs)).unwrap()
    _, docs_array = created_docs_

    # Fetch documents from db with bulk_get with revs param true
    bulk_get_: list[dict] = (await db.bulk_get(docs=docs_array, revs=True)).unwrap()
    doc_1, doc_2 = bulk_get_

    # FIXME: _revisions param must be included when bulk_docs is called with new_edits param False
    # Prepare list of documents to update with bulk_docs
    update_docs_: list[dict] = [
        {'_id': doc_1['docs'][0]['ok']['_id'],
         '_rev': doc_1['docs'][0]['ok']['_rev'],
         '_revisions': doc_1['docs'][0]['ok']['_revisions'],
         'name': 'John', 'email': 'peter@peter.com'
         },
        {'_id': doc_2['docs'][0]['ok']['_id'],
         '_rev': doc_2['docs'][0]['ok']['_rev'],
         '_revisions': doc_2['docs'][0]['ok']['_revisions'],
         'name': 'Steve', 'email': 'steve@steve.com'
         },
    ]

    # Update docs in batch with the bulk_docs function - new_edits param set to False,
    # prevents the database from assigning new revision IDs.
    update_docs_: tuple[CouchDatabase, list[dict]] = (await db.bulk_docs(update_docs_, False)).unwrap()
    _, update_docs_array = update_docs_
    updated_doc_1, updated_doc_2 = update_docs_array

    # Assert that new revs are not assigned
    assert doc_1['id'] == updated_doc_1['id'] and doc_2['id'] == updated_doc_2['id']
    assert doc_1['rev'] == updated_doc_1['rev'] and doc_2['rev'] == updated_doc_2['rev']


#
# find
#
@pytest.mark.asyncio
async def test_orm_database_find_ok(couch_setup, test_doc_list):
    '''
    This function tests find - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # create test docs
    bulk_docs_: tuple['CouchDatabase', list[dict]] = (await db.bulk_docs(test_doc_list)).unwrap() # Tuple
       
    # 1.
    # db.find when selector param is passed
    selector: dict = {'y': 15}

    find_: tuple[list[dict], str, str] = (await db.find(selector)).unwrap()
    docs_array_, _, _ = find_
    assert len(docs_array_) == 1

    # 2.
    # db.find when selector and limit param is passed
    selector: dict = {'name': 'John'}
    limit: int = 1

    find_: tuple[list[dict], str, str] = (await db.find(selector, limit)).unwrap()
    docs_array_, _, _ = find_
    assert len(docs_array_) == limit

    # 3.
    # db.find when selector and skip param is passed
    selector: dict = {'name': 'John'}
    skip: int = 1

    find_: tuple[list[dict], str, str] = (await db.find(selector, skip)).unwrap()
    docs_array_, _, _ = find_
    assert len(docs_array_) == 1

    # 4.
    # db.find when selector and sort param is passed
    # create index1
    index1: CouchIndex = db.index('y_desc')
    doc1: dict = {
        'fields': [
            {'y': 'desc'},
        ]
    }

    create_index_1: CouchIndex = (await index1.create(doc=doc1, name='y_desc')).unwrap()
         
    # db.find with index1
    selector: dict = {'y': {'$lt': 20}, 'name': 'John'}
    sort: list[dict] = [{'y': 'desc'}]

    find_: tuple[list[dict], str, str] = (await db.find(selector, sort=sort, use_index='y_desc')).unwrap()
    docs_array_, _, _ = find_
    assert len(docs_array_) == 1

    # create index2
    index2: CouchIndex = db.index('y_asc_name_asc')
    doc: dict = {
        'fields': [
            {'y': 'asc'},
            {'name': 'asc'},
        ]
    }

    (await index2.create(doc=doc)).unwrap()

    # db.find with index2
    selector: dict = {'y': {'$lt': 40}, 'name': 'John'}
    sort: list[dict] = [{'y': 'asc'}, {'name': 'asc'}]

    find_2: tuple[list[dict], str, str] = (await db.find(selector, sort=sort, use_index='y_asc_name_asc')).unwrap()
    docs_array_, _, _ = find_2
    assert len(docs_array_) == 2
    assert docs_array_[0]['y'] == 10

    # 5.
    # db.find when selector and fields param is passed
    selector: dict = {'name': 'John'}
    fields: list = ['name']

    find_: tuple[list[dict], str, str] = (await db.find(selector, fields=fields)).unwrap()
    docs_array_, _, _ = find_
    assert docs_array_[0]['name'] == 'John'

    # 6.
    # db.find when selector and r param is passed
    selector: dict = {'name': 'John'}
    find_: tuple[list[dict], str, str] = (await db.find(selector, r=1)).unwrap()

    # 7.
    # db.find when selector and bookmark param is passed
    selector: dict = {'name': 'John'}
    limit: int = 1
 
    find_: tuple[list[dict], str, str] = (await db.find(selector, limit=limit)).unwrap()
    docs_array_, _, _ = find_
    assert len(docs_array_) == limit
       
    find_2: tuple[list[dict], str, str] = (await db.find(selector, limit=limit, bookmark=find_[1])).unwrap()
    docs_array_2, _, _ = find_2
    assert len(docs_array_2) == limit 
    assert not all(map(lambda x, y: x == y, docs_array_, docs_array_2))

    # 8.
    # db.find when selector and update param is passed
    selector: dict = {'name': 'John'}

    # create index1
    index1: CouchIndex = db.index('y_desc2')
    doc1: dict = {
        'fields': [
            {'y': 'desc'},
        ]
    }

    (await index1.create(doc=doc1, name='y_desc2')).unwrap()
    find_: tuple[list[dict], str, str] = (await db.find(selector, update=False, use_index='y_desc2')).unwrap()

    # 9.
    # db.find when selector and stable param is passed
    selector: dict = {'name': 'John'}

    find_: tuple[list[dict], str, str] = (await db.find(selector, stable=True)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_database_find_wrong_limit_param(couch_setup):
    '''
    This function tests find - expected exception while given wrong limit param 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # create test docs
    bulk_docs_: tuple['CouchDatabase', list[dict]] = (await db.bulk_docs(test_doc_list)).unwrap() # Tuple

    selector: dict = {'name': 'John'}
    limit = '1'
    find_: tuple[list[dict], str, str] = (await db.find(selector, limit)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_database_find_wrong_skip_param(couch_setup):
    '''
    This function tests find - expected exception while given wrong skip param 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # create test docs
    bulk_docs_: tuple['CouchDatabase', list[dict]] = (await db.bulk_docs(test_doc_list)).unwrap() # Tuple

    selector: dict = {'name': 'John'}
    skip = '1'
    find_: tuple[list[dict], str, str] = (await db.find(selector, skip)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_database_find_wrong_r_param(couch_setup):
    '''
    This function tests find - expected exception while given wrong r param 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    # create test docs
    bulk_docs_: tuple['CouchDatabase', list[dict]] = (await db.bulk_docs(test_doc_list)).unwrap() # Tuple

    selector: dict = {'name': 'John'}
    find_: tuple[list[dict], str, str] = (await db.find(selector, r='1')).unwrap()

  
#
# create_index
#
@pytest.mark.asyncio
async def test_orm_database_create_index_ok(couch_setup):
    '''
    This function tests create index - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    index: CouchIndex = db.index(name='test_index')
    assert isinstance(index, CouchIndex)

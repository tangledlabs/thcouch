import pytest

from thresult import ResultException

from thcouch.orm.attachment import CouchAttachment
from thcouch.orm.document import CouchDocument
from thcouch.core.doc import put_doc, PutDocOk
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase


# get
@pytest.mark.asyncio
async def test_orm_attachement_get(couch_setup):
    '''
    This function tests create attachement - orm - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()   
    document = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document, attachment_name='attachment_name')
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    couch_attachment: CouchAttachment = (await attachment.update(docid='0',
                                                                 attachment_name=file_name,
                                                                 rev=rev,
                                                                 body=content)).unwrap()   
        
    couch_attachment: CouchAttachment = (await attachment.get(docid='0', attname=file_name)).unwrap()
    assert isinstance(couch_attachment, CouchAttachment)
    assert isinstance(couch_attachment.document, CouchDocument)            


# update
@pytest.mark.asyncio
async def test_orm_attachement_update(couch_setup):
    '''
    This function tests create attachement - orm - Success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document, attachment_name='attachment_name')
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI,
                                   db=COUCH_DATABASE,
                                   docid='1',
                                   doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    res_tuple: tuple[CouchDocument, CouchAttachment] = (await attachment.update(docid='1',
                                                                 attachment_name=file_name,
                                                                 rev=rev,
                                                                 body=content)).unwrap()

    couch_document, couch_attachment = res_tuple
    assert isinstance(couch_document, CouchDocument)
    assert isinstance(couch_attachment, CouchAttachment)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_orm_attachement_get_wrong_rev(couch_setup):
    '''
    This function tests get attachement - expected exception while given wrong rev
    '''
    
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap() 
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document,
                                                  attachment_name='attachment_name',
                                                  digest='digest',
                                                  length=200,
                                                  revpos=100,
                                                  stub=True)
    
    doc:PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    couch_attachment: CouchAttachment = (await attachment.update(docid='0',
                                                                 attachment_name=file_name,
                                                                 rev=rev,
                                                                 body=content)).unwrap()
    
    couch_attachment: CouchAttachment = (await attachment.get(docid='0', attname=file_name, rev='wrong_rev')).unwrap()
    
    
@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_orm_attachement_get_wrong_attachment_name(couch_setup):
    '''
    This function tests get attachement - expected exception while given wrong attachment name
    '''
    
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document,
                                                  attachment_name='attachment_name',
                                                  digest='digest',
                                                  length=200,
                                                  revpos=100,
                                                  stub=True)
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    couch_attachment: CouchAttachment = (await attachment.update(docid='0',
                                                                 attachment_name=file_name,
                                                                 rev=rev,
                                                                 body=content)).unwrap()
    
    couch_attachment: CouchAttachment = (await attachment.get(docid='0', attname='wrong name')).unwrap()
    
    
@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_orm_attachement_get_wrong_id(couch_setup):
    '''
    This function tests get attachement - expected exception while given wrong id
    '''
    
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document,
                                                  attachment_name='attachment_name',
                                                  digest='digest',
                                                  length=200,
                                                  revpos=100,
                                                  stub=True)
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    couch_attachment: CouchAttachment = (await attachment.update(docid='0',
                                                                 attachment_name=file_name,
                                                                 rev=rev,
                                                                 body=content)).unwrap()
    
    couch_attachment: CouchAttachment = (await attachment.get(docid='wrong id', attname=file_name)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_orm_attachement_update_wrong_rev(couch_setup):
    '''
    This function tests update attachement - expected exception while given wrong rev
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document,
                                                  attachment_name='attachment_name',
                                                  digest='digest',
                                                  length=200,
                                                  revpos=100,
                                                  stub=True)
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI,
                                   db=COUCH_DATABASE,
                                   docid='0',
                                   doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    couch_attachment: CouchAttachment = (await attachment.update(docid='0',
                                                                 attachment_name=file_name,
                                                                 rev='wrong_rev',
                                                                 body=content)).unwrap()
    

@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_orm_attachement_update_wrong_id(couch_setup):
    '''
    This function tests update attachement - expected exception while given wrong id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()   
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document,
                                                  attachment_name='attachment_name',
                                                  digest='digest',
                                                  length=200,
                                                  revpos=100,
                                                  stub=True)
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI,
                                   db=COUCH_DATABASE,
                                   docid='0',
                                   doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    couch_attachment: CouchAttachment = (await attachment.update(docid='wrong_id',
                                                                 attachment_name=file_name,
                                                                 rev=rev,
                                                                 body=content)).unwrap()
    
    
@pytest.mark.asyncio
async def test_orm_attachement_update_wrong_attachment_name(couch_setup):
    '''
    This function tests update attachement - expected exception while given wrong attachment name
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()   
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document,
                                                  attachment_name='attachment_name',
                                                  digest='digest',
                                                  length=200,
                                                  revpos=100,
                                                  stub=True)
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI,
                                   db=COUCH_DATABASE,
                                   docid='0',
                                   doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    res_tuple: tuple[CouchDocument, CouchAttachment] = (await attachment.update(docid='0',
                                                                 attachment_name='wrong_name',
                                                                 rev=rev,
                                                                 body=content)).unwrap()

    couch_document, couch_attachment = res_tuple
    assert isinstance(couch_document, CouchDocument)
    assert isinstance(couch_attachment, CouchAttachment)
    
    
@pytest.mark.asyncio
async def test_orm_attachement_update_wrong_body(couch_setup):
    '''
    This function tests update attachement - expected exception while given wrong body
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document,
                                                  attachment_name='attachment_name',
                                                  digest='digest',
                                                  length=200,
                                                  revpos=100,
                                                  stub=True)
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI,
                                   db=COUCH_DATABASE,
                                   docid='0',
                                   doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    res_tuple: tuple[CouchDocument, CouchAttachment] = (await attachment.update(docid='0',
                                                                 attachment_name='file_name',
                                                                 rev=rev,
                                                                 body='content')).unwrap()

    couch_document, couch_attachment = res_tuple
    assert isinstance(couch_document, CouchDocument)
    assert isinstance(couch_attachment, CouchAttachment)
            


# remove
@pytest.mark.asyncio
async def test_orm_attachement_remove(couch_setup):
    '''
    This function tests remove attachement - orm - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document, attachment_name='attachment_name')
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    res_tuple: tuple[CouchDocument, CouchAttachment] = (await attachment.update(docid='0',
                                                                 attachment_name=file_name,
                                                                 rev=rev,
                                                                 body=content)).unwrap()
    
    couch_document, _ = res_tuple
    rev = couch_document['_rev']

    deleted_atachment: tuple[CouchDocument, CouchAttachment] = (await attachment.remove(docid='0', attname=file_name, rev=rev)).unwrap()
    
    doc, att = deleted_atachment
    assert isinstance(doc, CouchDocument)
    assert isinstance(att, CouchAttachment)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_orm_attachement_remove_wrong_rev(couch_setup):
    '''
    This function tests remove attachement - expected exception while given wrong rev
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document, attachment_name='attachment_name')
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    (await attachment.update(docid='0',
                             attachment_name=file_name,
                             rev=rev,
                             body=content)).unwrap()

    (await attachment.remove(docid='0', attname=file_name, rev='wrong_rev')).unwrap()


@pytest.mark.asyncio
async def test_orm_attachement_remove_wrong_batch(couch_setup):
    '''
    This function tests remove attachement - orm - wrong batch - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document, attachment_name='attachment_name')
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    res_tuple: tuple[CouchDocument, CouchAttachment] = (await attachment.update(docid='0',
                                                        attachment_name=file_name,
                                                        rev=rev,
                                                        body=content)).unwrap()
    
    couch_document, _ = res_tuple
    rev = couch_document['_rev']

    deleted_atachment: tuple[CouchDocument, CouchAttachment] = (await attachment.remove(docid='0', attname=file_name, rev=rev, batch='wrong_batch')).unwrap()
    
    doc, att = deleted_atachment
    assert isinstance(doc, CouchDocument)
    assert isinstance(att, CouchAttachment)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_orm_attachement_remove_wrong_docid(couch_setup):
    '''
    This function tests remove attachement - expected exception while given wrong docid
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document, attachment_name='attachment_name')
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    res_tuple: tuple[CouchDocument, CouchAttachment] = (await attachment.update(docid='0',
                                                        attachment_name=file_name,
                                                        rev=rev,
                                                        body=content)).unwrap()
    
    couch_document, _ = res_tuple
    rev = couch_document['_rev']

    (await attachment.remove(docid='wrong_id', attname=file_name, rev=rev)).unwrap()
    
    
@pytest.mark.asyncio
async def test_orm_attachement_remove_wrong_attachment_name(couch_setup):
    '''
    This function tests remove attachement - orm - wrong attachmen name - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup  
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()    
    document: CouchDocument = CouchDocument(database=db)
    
    attachment: CouchAttachment = CouchAttachment(document=document, attachment_name='attachment_name')
    
    doc: PutDocOk = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()
        
    res_tuple: tuple[CouchDocument, CouchAttachment] = (await attachment.update(docid='0',
                                                        attachment_name=file_name,
                                                        rev=rev,
                                                        body=content)).unwrap()
    
    couch_document, _ = res_tuple
    rev = couch_document['_rev']

    deleted_atachment: tuple[CouchDocument, CouchAttachment] = (await attachment.remove(docid='0', attname='file_name', rev=rev)).unwrap()
    
    doc, att = deleted_atachment
    assert isinstance(doc, CouchDocument)
    assert isinstance(att, CouchAttachment)
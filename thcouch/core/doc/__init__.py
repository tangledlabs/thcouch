from .get import *
from .head import *
from .put import *
from .delete import *